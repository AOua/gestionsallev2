package fr.afpa.iservicemetier;

import java.util.List;

import fr.afpa.entitemetier.Batiment;

public interface IServiceBatimentMetier {

	public List<Batiment> getListBatimentMetier();
	public Batiment ajoutBatiment(Batiment batiment);
	public Batiment getBatiment(Long id);
	public Batiment updateBatiment(Batiment batiment);
}
