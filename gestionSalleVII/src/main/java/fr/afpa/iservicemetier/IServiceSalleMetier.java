package fr.afpa.iservicemetier;

import java.util.List;

import fr.afpa.entitemetier.TypeSalle;


import fr.afpa.entitemetier.Materiel;
import fr.afpa.entitemetier.Salle;



public interface IServiceSalleMetier {
	

	//boolean creationSalle(String nom, Long batiment, Long type, boolean reserve);

	boolean creationSalle(String nom, Long batiment, Long type, List<Materiel> listeMateriel);

	boolean modificationSalle(String nom, Long batiment, Long type, boolean reserve); 
	
	public List<TypeSalle> getListTypeSalle();
	public TypeSalle getTypeSalle(Long id);

	public List<Salle> afficherListeSalle();  
	
	public Salle getSalle(Long id);

	
	TypeSalle creationTypeSalle(TypeSalle typeSalle);

	public Salle afficherSalle(String nom);

	public boolean modificationSalle(String nom,Long idSalle, Long batiment, Long type, List<Materiel> listeMateriel);

	public Salle creationSalleRest(Salle salle);

	public TypeSalle modificationTypeSalle(TypeSalle typeSalle);  
	
	public Salle modificationSalle(Salle salle); 
}
