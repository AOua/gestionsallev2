package fr.afpa.iservicemetier;

import java.util.List;


import fr.afpa.entitemetier.TypeMateriel;

public interface IServiceMaterielMetier {

	public TypeMateriel ajoutTypeMateriel(TypeMateriel typeMateriel);

	public List<TypeMateriel> afficherListeMateriels();
	
	public TypeMateriel getMateriel(Long id);

	public TypeMateriel deleteById(long id);

	public TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel);  
}
