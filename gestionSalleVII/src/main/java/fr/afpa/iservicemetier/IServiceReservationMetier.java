package fr.afpa.iservicemetier;

import java.util.List;

import fr.afpa.entitemetier.Reservation;

public interface IServiceReservationMetier {

	public Reservation ajoutReservation(Reservation reservation, Long personne, Long salle);
	
	public List<Reservation> getListReservation(Long id);
	
	public List<Reservation> getListeReservations();
	
	public Reservation getReservation(Long id);

	public Reservation modifierReservation(Reservation reservation, Long idSalle);
}
