package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.BatimentDao;

public interface BatimentRepository extends JpaRepository<BatimentDao, Long>{

}
