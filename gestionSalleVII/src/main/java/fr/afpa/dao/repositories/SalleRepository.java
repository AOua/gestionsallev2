package fr.afpa.dao.repositories;



import org.springframework.data.jpa.repository.JpaRepository;


import fr.afpa.entitedao.SalleDao;

public interface SalleRepository extends JpaRepository<SalleDao, Long> {

	SalleDao findByNom(String nom); 

}
