package fr.afpa.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.ReservationDao;

public interface ReservationRepository  extends JpaRepository<ReservationDao, Long> {

	List<ReservationDao> findByPersonneIdPersonne(Long id);
}
