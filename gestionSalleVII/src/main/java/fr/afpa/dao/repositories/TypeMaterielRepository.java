package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.TypeMaterielDao;

public interface TypeMaterielRepository extends JpaRepository<TypeMaterielDao, Long>{

}
