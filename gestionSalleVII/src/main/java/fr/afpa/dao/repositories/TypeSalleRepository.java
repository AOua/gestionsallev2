package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.TypeSalleDao;
import fr.afpa.entitemetier.TypeSalle;

public interface TypeSalleRepository extends JpaRepository<TypeSalleDao, Long>{

	public TypeSalle saveAndFlush(TypeSalle typeSalle);

}
