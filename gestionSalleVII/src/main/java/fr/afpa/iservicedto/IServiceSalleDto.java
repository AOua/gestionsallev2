package fr.afpa.iservicedto;

import java.util.List;



import fr.afpa.entitemetier.Batiment;

import fr.afpa.entitemetier.Salle;
import fr.afpa.entitemetier.TypeSalle;

public interface IServiceSalleDto {
	
	
	boolean creationSalle(Salle salleMetier);
	
	boolean modificationSalle(Salle salle, Batiment batimentMetier, TypeSalle typeSalleMetier); 

	//boolean creationSalle(Salle salle);
	
	public Batiment rechercheBatiment (Long batiment);

	public TypeSalle rechercheTypeSalle(Long type);

	
	public Salle creationSalleRest(Salle salle);
	

	public Salle rechercheSalle(String nom);

	public List<TypeSalle> getListSalleDto();
	public List<Salle> listerToutesLesSalles(); 



	public TypeSalle addTypeSalle(TypeSalle typeSalle);
	
	// � supprimer 
	//public Salle afficherSalle();

	public Salle afficherSalle(String nom);
	
	public Salle getSalle(Long id);

	public boolean modificationSalle(Salle salle);

	public TypeSalle modificationTypeSalle(TypeSalle typeSalle);
	
	public Salle modificationSalleRest(Salle salle);

}
