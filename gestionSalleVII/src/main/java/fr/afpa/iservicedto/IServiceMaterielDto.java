package fr.afpa.iservicedto;

import java.util.List;

import fr.afpa.entitemetier.TypeMateriel;

public interface IServiceMaterielDto {

	
	public TypeMateriel addTypeMaterielDao(TypeMateriel typeMateriel);
	
	public List<TypeMateriel> afficherListeMateriels();
	
	public TypeMateriel getMateriel(Long id);

	public TypeMateriel deleteMateriel(long id); 
	
	public TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel);

	
}
