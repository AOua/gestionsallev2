package fr.afpa.iservicedto;

import java.util.List;

import fr.afpa.entitemetier.Batiment;

public interface IServiceBatimentDto {
	
 public List<Batiment> getListBatimentDto();
 public Batiment addBatiment(Batiment batiment);
 public Batiment getBatiment(Long id);
 public Batiment updateBatiment (Batiment batiment);
 
}
