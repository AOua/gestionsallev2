package fr.afpa.iservicedto;

import java.util.List;

import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Reservation;
import fr.afpa.entitemetier.Salle;

public interface IServiceReservationDto {

	public Reservation ajoutReservation(Reservation reservation, Personne personne, Salle salle);
	
	public List<Reservation> getListReservation(Long id);
	public List<Reservation> getListeReservations();
	public Reservation getReservation(Long id);

	public Reservation modifierReservation(Reservation reservation, Salle salle);
}
