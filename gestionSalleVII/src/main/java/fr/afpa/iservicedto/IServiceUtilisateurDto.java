package fr.afpa.iservicedto;


import java.util.List;

import fr.afpa.entitedao.PersonneDao;

import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Role;

public interface IServiceUtilisateurDto {

	public List<Personne> listerTousLesUtilisateurs();
	
	
	public PersonneDao modifUtilisateur(Personne personneMetier);


	public Personne creationUtilisateur(Personne personne);

	public Role rechercheRole(Long role);
	
	public List<Personne> getListFormateur(Long id);
	
	public Personne getPersonne(Long id);


	//public boolean modificationUtulisateur(Personne personne, Authentification authentification, Role roleMetier);


	//public boolean modificationUtulisateur(Long id);


	//public Personne modificationUtilisateur(Long id);


	public Personne modificationUtilisateur(Long id);


	public Personne modificationUtilisateur(Personne personne);


	//PersonneDao modificationUtilisateur(Long id); 
	
}
