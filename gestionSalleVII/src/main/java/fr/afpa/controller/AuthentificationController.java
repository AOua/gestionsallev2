package fr.afpa.controller;



import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entitemetier.Authentification;
import fr.afpa.entitemetier.Personne;
import fr.afpa.iservicemetier.IServiceAuthentificationMetier;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;



@Controller

public class AuthentificationController {
	@Autowired
	private IServiceAuthentificationMetier servauth;
	
	@Autowired
	private IServiceUtilisateurMetier servUser;
	
	/**
	 * Page principal au d�marrage
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "home";
	}
	
	/**
	 * Controller permettant de g�rer l'authentification admin, formateur ou stagiaire
	 * @param login
	 * @param password
	 * @param session
	 * @return
	 */
	@PostMapping( value = "/firstLogin")
	public ModelAndView authentificate(@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		
		
		    String error = "Login ou mot de passe incorrect";
		    
			Authentification auth = servauth.getAdminOrUser(login, password);
			List<Personne> listePersonne = servUser.afficherListeUtilisateur();
			
			if(auth != null) {
				
				for (Personne personne : listePersonne) {
					
					if(personne.getAuthentification().getIdAuthentification() == auth.getIdAuthentification() && personne.getRole().getIdRole()==1) {
						session.setAttribute("admin", personne);
						mv.setViewName("menuAdmin");
						break;
						
					}
					else if(personne.getAuthentification().getIdAuthentification() == auth.getIdAuthentification() && personne.getRole().getIdRole()==2) {
						session.setAttribute("formateur", personne);
						mv.setViewName("panelFormateur");
						break;
						
					}
					else if(personne.getAuthentification().getIdAuthentification() == auth.getIdAuthentification() && personne.getRole().getIdRole()==3) {
						session.setAttribute("stagiaire", personne);
						mv.setViewName("panelStagiaire");
						break;
					}		
				}
				
			}
			else {
				mv.setViewName("home");
			}

		return mv;
		
	}

	
	/**
	 * deconnexion pour vider la session
	 * @param mv
	 * @param session
	 * @return
	 */
	@GetMapping(value="/deconnexion")
	public ModelAndView deconnexion(ModelAndView mv, HttpSession session) {
		session.invalidate();
		mv.setViewName("home");
		return mv;
	}
}
