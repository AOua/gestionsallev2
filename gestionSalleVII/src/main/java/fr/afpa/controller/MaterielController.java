package fr.afpa.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entitemetier.TypeMateriel;
import fr.afpa.iservicemetier.IServiceMaterielMetier;

@Controller
public class MaterielController {

	@Autowired
	private IServiceMaterielMetier servMateriel;
	
	/**
	 * redirection creation du type de materiel
	 * @param mv
	 * @return
	 */
	@GetMapping(value = "/creationTypeMateriel")
	public ModelAndView creationTypeMaterielGet(ModelAndView mv) {
		
		
		mv.setViewName("creationTypeMateriel");
		return mv;
	}
	
	/**
	 * creation du type de materiel apres validation
	 * @param mv
	 * @param nom
	 * @return
	 */
	@PostMapping(value = "/creationTypeMateriel", params = { "nom" })
	public ModelAndView creationTypeMaterielPost(ModelAndView mv, @RequestParam(value = "nom") String nom) {

		TypeMateriel typeMateriel = new TypeMateriel();
		typeMateriel.setLibelle(nom);
		servMateriel.ajoutTypeMateriel(typeMateriel);
		mv.setViewName("menuAdmin");
		return mv;
	}
	
	
}
