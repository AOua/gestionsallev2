package fr.afpa.controller;



import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.dao.repositories.UtilisateurRepository;

import fr.afpa.entitemetier.Admin;
import fr.afpa.entitemetier.Authentification;
import fr.afpa.entitemetier.Formateur;
import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Role;
import fr.afpa.entitemetier.Stagiaire;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;

@Controller
public class UtilisateurController {
	
	@Autowired
	private IServiceUtilisateurMetier servMetier;
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	/**
	 * Afficher liste des utilisateurs
	 * @return
	 */
	@GetMapping(value = "/listerPersonnes")
	public ModelAndView afficherListeUtilisateur(){
		ModelAndView mv = new ModelAndView();
		
			ArrayList<Personne> personnes = (ArrayList<Personne>) servMetier.afficherListeUtilisateur();
			
			mv.setViewName("afficherListeUtilisateur");
			mv.addObject("personnes", personnes);
		
		return mv;
	}
/**
 * Modifie les infos d'un utilisateur
 * @param login
 * @param password
 * @param nom
 * @param prenom
 * @param tel
 * @param mail
 * @param adresse
 * @return
 */
	@GetMapping(value ="/modificationUtilisateur")
	public ModelAndView modificationUtilisateurGet(HttpSession session) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("modificationUtilisateur");
		return mv;
	}
	
	@PostMapping(value="/modificationUtilisateur",params = { "id"})
	public ModelAndView modificationUtilisateurPost( @RequestParam(value = "id") Long id, HttpSession session) {
		
		ModelAndView mv = new ModelAndView();
		
		mv.setViewName("modificationUtilisateur");
	//PersonneDao pers=	utilisateurRepository.findById(id).get();
		Personne pers = servMetier.getPersonne(id);
		mv.addObject("personne",pers);
	//return	servMetier.modificationPersonne(id);
		return mv;
	}
	
	@GetMapping(value="/ARC")
	public ModelAndView voirUtilisateurGet(HttpSession session) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("voirUtilisateur");
		return mv;
	}
	
	@PostMapping(value="/ARC", params = {"id"})
	public ModelAndView voirUtilisateurPost( @RequestParam(value="id") Long id, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("voirUtilisateur");
		Personne p = servMetier.getPersonne(id);
		mv.addObject("personne",p);
		return mv;
	}
	
	@PostMapping(value = "/m")
	public ModelAndView modif(@RequestParam(value = "id") String id, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "mail") String email, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "adresse") String adresse, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp, @RequestParam(value="role") String role) {
		Personne personne = null;
		Authentification auth = new Authentification(login, mdp);
		
		switch (role) {
		case "1":
			personne = new Admin(nom, prenom, email, tel, adresse, auth, new Role(1l, "Admin"));
			personne.setId(Long.parseLong(id));
			break;
		case "2":
			personne = new Formateur(nom, prenom, email, tel, adresse, auth, new Role(2l, "Formateur"));
			personne.setId(Long.parseLong(id));
			
			break;
		case "3":
			personne = new Stagiaire(nom, prenom, email, tel, adresse, auth, new Role(3l, "Stagiaire"));
			personne.setId(Long.parseLong(id));
			
			break;

		default:
			break;
		}
		
		servMetier.modificationPersonne(personne);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("afficherListeUtilisateur");
		mv.addObject("personnes", (ArrayList<Personne>) servMetier.afficherListeUtilisateur());
		return mv;
	}
	
	@GetMapping(value = "/creation")
	public ModelAndView creationUtilisateurGet(ModelAndView mv) {

		mv.setViewName("creationUtilisateur");
		return mv;
	}
/*
	@PostMapping(value = "/creation", params = { "nom", "prenom", "email", "tel",
			"adresse", "login", "mdp","role"})
	public ModelAndView creationUtilisateurPost(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "email") String email, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "adresse") String adresse, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp, @RequestParam(value="role") String role, HttpSession session) {

		
		//� completer avec une recherche de personne pour voir si elle n'existe pas d�j� en base 
	
		ModelAndView mv1 = new ModelAndView();
		
		Authentification authentification = new Authentification();
		authentification.setLogin(login);
		authentification.setMotDePasse(mdp);
		servMetier.creationPersonne(nom, prenom, email, tel, adresse, Long.parseLong(role), authentification);
		return afficherListeUtilisateur();
		
		
		//� verifier et completer avec une page jsp de confirmation de creation compte ou une redirection 
		
	}*/
	
	@PostMapping(value="/Delete")
	public ModelAndView deleteUtilisateur(@RequestParam(value="id") int id, HttpSession session) {

		Long newId = (long) id;
		if(newId != 1) {
			utilisateurRepository.deleteById(newId);
		}
		
		return afficherListeUtilisateur();
		
	}
	
	
}
