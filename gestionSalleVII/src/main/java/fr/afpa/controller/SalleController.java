package fr.afpa.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entitemetier.Batiment;
import fr.afpa.entitemetier.Materiel;
import fr.afpa.entitemetier.Salle;
import fr.afpa.entitemetier.TypeMateriel;
import fr.afpa.entitemetier.TypeSalle;
import fr.afpa.iservicemetier.IServiceBatimentMetier;
import fr.afpa.iservicemetier.IServiceMaterielMetier;
import fr.afpa.iservicemetier.IServiceSalleMetier;

@Controller
public class SalleController {

	@Autowired
	private IServiceSalleMetier servSalleMetier;

	@Autowired
	private IServiceBatimentMetier servBatimentMetier;

	@Autowired
	private IServiceMaterielMetier servMateriel;

	
	
	/**
	 * Pour creer une salle en resigant le nom , le type de salle et le batiment
	 * 
	 * @param mv
	 * @return
	 */
/*
	@GetMapping(value = "/creationSalle")
	public ModelAndView creationSalleGet(ModelAndView mv) {

		List<Batiment> listBatiment = servBatimentMetier.getListBatimentMetier();
		List<TypeSalle> listTypeSalle = servSalleMetier.getListTypeSalle();
		List<TypeMateriel> listTypeMateriel = servMateriel.afficherListeMateriels();
		mv.addObject("listTypeSalle", listTypeSalle);
		mv.addObject("listBatiment", listBatiment);
		mv.addObject("listTypeMateriel", listTypeMateriel);
		mv.setViewName("creationSalle");
		return mv;
	}

	@PostMapping(value = "/creationSalle")
	public ModelAndView creationSallePost(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "selectBatiment") Long batiment, @RequestParam(value = "selectTypeSalle") Long type,
			@RequestParam(value = "listeNombreMateriel[]") Integer[] listeNombreMateriel,
			@RequestParam(value = "listeNomMateriel[]") String[] listeNomMateriel,
			@RequestParam(value = "listeIdMateriel[]") Long[] listeIdMateriel) {

		// � completer avec une recherche de salle pour voir si elle n'existe pas
		// d�j�
		// en base

		List<Materiel> listeMateriel = new ArrayList<Materiel>();
		int nb = listeNomMateriel.length;
		for (int i = 0; i < nb; i++) {
			Materiel materiel = new Materiel();
			materiel.setId(listeIdMateriel[i]);
			materiel.setNom(listeNomMateriel[i]);
			materiel.setQuantite(listeNombreMateriel[i]);
			listeMateriel.add(materiel);

		}

		ModelAndView mv1 = new ModelAndView();

		mv1.setViewName("creationSalle");

		servSalleMetier.creationSalle(nom, batiment, type, listeMateriel);

		// � verifier et completer avec une page jsp de confirmation de creation
		// compte
		// ou une redirection
		return mv1;
	}*/

	/**
	 * methode pour modifier une salle
	 * 
	 * @param model and view v3
	 * @return
	 */

	@PostMapping(value = "/modificationSalle")
	public ModelAndView modificationSallePost(HttpSession session, @RequestParam(value = "id") Long id) {
		Salle salle = servSalleMetier.getSalle(id);
		List<TypeMateriel> listTypeMateriel = servMateriel.afficherListeMateriels();
		ModelAndView mv = new ModelAndView();
		
		
		listTypeMateriel.forEach(m->{
		if(!salle.getListMateriel().stream()
				.filter(sm->sm.getId()==m.getId())
				.findFirst().isPresent()) {
			salle.getListMateriel().add(new Materiel(m.getId(), m.getLibelle(), 0));
		}
		});

		mv.addObject("salle", salle);
		List<Batiment> listBatiment = servBatimentMetier.getListBatimentMetier();
		List<TypeSalle> listTypeSalle = servSalleMetier.getListTypeSalle();
		
		mv.addObject("listTypeSalle", listTypeSalle);
		mv.addObject("listBatiment", listBatiment);
		mv.addObject("listTypeMateriel", listTypeMateriel);
		System.out.println("****************" + salle.getNom());
		mv.setViewName("modificationSalle");
		return mv;
	}

	@PostMapping(value = "/modificationSalleForm")
	public ModelAndView modificationSalleForm(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "selectBatiment") Long batiment, @RequestParam(value = "selectTypeSalle") Long type,
			@RequestParam(value = "listeNombreMateriel[]") Integer[] listeNombreMateriel,
			@RequestParam(value = "listeNomMateriel[]") String[] listeNomMateriel,
			@RequestParam(value = "listeIdMateriel[]") Long[] listeIdMateriel, @RequestParam(value="idsalle") Long idSalle) {

		List<Materiel> listeMateriel = new ArrayList<Materiel>();
		int nb = listeNomMateriel.length;
		for (int i = 0; i < nb; i++) {
			Materiel materiel = new Materiel();
			materiel.setId(listeIdMateriel[i]);
			materiel.setNom(listeNomMateriel[i]);
			materiel.setQuantite(listeNombreMateriel[i]);
			listeMateriel.add(materiel);
			System.out.println(listeIdMateriel[i]);
		}
		ModelAndView mv1 = new ModelAndView();

		mv1.setViewName("modificationSalle");
		
		System.out.println(idSalle);
		servSalleMetier.modificationSalle(nom, idSalle, batiment, type, listeMateriel);

		// � verifier et completer avec une page jsp de confirmation de creation
		// compte
		// ou une redirection
		return mv1;
	}

	/**
	 * methode pour afficher la liste des salles
	 * 
	 * @return une liste de salles
	 */

	@GetMapping(value = "/afficherListeSalle")
	public ModelAndView afficherListeSalle() {
		List<Salle> salles = (List<Salle>) servSalleMetier.afficherListeSalle();
		ModelAndView mv4 = new ModelAndView();
		mv4.setViewName("afficherListeSalle");
		mv4.addObject("salles", salles);
		return mv4;
	}

	/**
	 * pour creer un nouveau type de salles
	 * 
	 * @param model and view
	 * @return
	 */

	@GetMapping(value = "/creationTypeSalle")
	public ModelAndView creationTypeSalleGet(ModelAndView mv) {

		mv.setViewName("creationTypeSalle");
		return mv;
	}

	@PostMapping(value = "/creationTypeSalle", params = { "nom" })
	public ModelAndView creationTypeSallePost(ModelAndView mv, @RequestParam(value = "nom") String nom) {

		TypeSalle typeSalle = new TypeSalle();
		typeSalle.setLibelle(nom);

		ModelAndView mv1 = new ModelAndView();
		mv1.setViewName("menuAdmin");
		servSalleMetier.creationTypeSalle(typeSalle);

		return mv1;
	}

	@GetMapping(value = "/afficherSalle")
	public ModelAndView afficherSalleGet(ModelAndView mv5) {
		mv5.setViewName("afficherSalle");
		return mv5;
	}

	@PostMapping(value = "afficherSalle")
	public ModelAndView afficherSallePost(ModelAndView mv, @RequestParam(value = "nom") String nom) {
		Salle salle = servSalleMetier.afficherSalle(nom);
		salle.getNom().equals(nom);
		ModelAndView mv5 = new ModelAndView();
		mv5.setViewName("afficherSalle");
		mv5.addObject("salle", salle);
		return mv5;
	}

}
