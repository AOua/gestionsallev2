package fr.afpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entitemetier.Batiment;

import fr.afpa.iservicemetier.IServiceBatimentMetier;


@Controller
public class BatimentController {

	@Autowired
	private IServiceBatimentMetier servBatiment;
	
	/**
	 * redirection vers le formulaire de cr�ation de batiment
	 * @param mv
	 * @return
	 */
	@GetMapping(value = "/creationBatiment")
	public ModelAndView creationBatimentGet(ModelAndView mv) {
		
		
		mv.setViewName("creationBatiment");
		return mv;
	}
	
	/**
	 * creation du batiment
	 * @param mv
	 * @param nom
	 * @return
	 */
	@PostMapping(value = "/creationBatiment", params = { "nom" })
	public ModelAndView creationBatimentPost(ModelAndView mv, @RequestParam(value = "nom") String nom) {

		Batiment batiment = new Batiment();
		batiment.setNom(nom);
		servBatiment.ajoutBatiment(batiment);
		mv.setViewName("menuAdmin");
		return mv;
	}
}
