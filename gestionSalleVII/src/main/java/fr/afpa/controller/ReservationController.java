package fr.afpa.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Reservation;
import fr.afpa.entitemetier.Salle;
import fr.afpa.iservicemetier.IServiceReservationMetier;
import fr.afpa.iservicemetier.IServiceSalleMetier;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;

@Controller
public class ReservationController {
	
	@Autowired
	private IServiceSalleMetier servSalleMetier;
	
	@Autowired
	private IServiceReservationMetier servResMetier;
	
	@Autowired
	private IServiceUtilisateurMetier servUtilisateurMetier;
	
	/**
	 * Redirection au formulaire de reservation avec les donn�es de salle 
	 * et de personne pr�sent en bdd
	 * @param mv
	 * @return
	 */
	@GetMapping(value="/reserver")
	public ModelAndView goToReservationForm(ModelAndView mv) {
		Long id = (long) 2;
		List<Personne> listPersonne = servUtilisateurMetier.getListFormateur(id);
		List<Salle> listSalle = servSalleMetier.afficherListeSalle();
		mv.addObject("listPersonne", listPersonne );
		mv.addObject("listSalle", listSalle);
		mv.setViewName("reservation");
		return mv;
	}
	
	@PostMapping(value="/modifierReservationAdminForm")
	public ModelAndView goToReservationFormAdmin(ModelAndView mv, @RequestParam(value="idReservation") Long idReservation) {
		List<Salle> listSalle = servSalleMetier.afficherListeSalle();
		mv.addObject("idreservation", idReservation);
		mv.addObject("listSalle", listSalle);
		mv.setViewName("modifierReservationAdminForm");
		return mv;
	}
	
	/**
	 * Cr�ation de la reservation
	 * @param mv
	 * @param nom
	 * @param dateReservation
	 * @param selectPersonne
	 * @param selectSalle
	 * @return
	 */
	@PostMapping(value="/creationReservation")
	public ModelAndView reservation(ModelAndView mv, @RequestParam(value="nom") String nom, @RequestParam(value="dateReservationDebut") String dateReservationDebut, @RequestParam(value="dateReservationFin") String dateReservationFin, @RequestParam(value="selectPersonne") int selectPersonne, @RequestParam(value="selectSalle") int selectSalle) {
		Reservation reservation = new Reservation();
		Long idPersonne = (long) selectPersonne;
		Long idSalle = (long) selectSalle;
		reservation.setMotif(nom);
		String dateDebut = dateReservationDebut;
		DateTimeFormatter  formatterDebut = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldDebut= LocalDate.parse(dateDebut, formatterDebut);
		reservation.setDateDebut(ldDebut);
		String dateFin = dateReservationFin;
		DateTimeFormatter  formatterFin = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldFin= LocalDate.parse(dateFin, formatterFin);
		reservation.setDateFin(ldFin);
		if(ldDebut.isBefore(ldFin)) {
			servResMetier.ajoutReservation(reservation, idPersonne, idSalle);	
			mv.setViewName("menuAdmin");
		}else {
			goToReservationForm(mv);
		}
		return mv;
	}
	
	@PostMapping(value="/formModifierReservation")
	public ModelAndView ModifierReservation(ModelAndView mv, @RequestParam(value="nom") String nom, @RequestParam(value="dateReservationDebut") String dateReservationDebut, @RequestParam(value="dateReservationFin") String dateReservationFin, @RequestParam(value="selectSalle") int selectSalle, @RequestParam(value="idReservation") Long idReservation) {
		Reservation reservation = new Reservation();
		Long idSalle = (long) selectSalle;
		reservation.setIdReservation(idReservation);
		reservation.setMotif(nom);
		String dateDebut = dateReservationDebut;
		DateTimeFormatter  formatterDebut = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldDebut= LocalDate.parse(dateDebut, formatterDebut);
		reservation.setDateDebut(ldDebut);
		String dateFin = dateReservationFin;
		DateTimeFormatter  formatterFin = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldFin= LocalDate.parse(dateFin, formatterFin);
		reservation.setDateFin(ldFin);
		if(ldDebut.isBefore(ldFin)) {
			servResMetier.modifierReservation(reservation, idSalle);	
			mv.setViewName("menuAdmin");
		}else {
			goToReservationForm(mv);
		}
		return mv;
	}
	
	/**
	 * voir la liste des reservations selon le formateur qui se connecte
	 * @param mv
	 * @param session
	 * @return
	 */
	@GetMapping(value="/voirReservation")
	public ModelAndView voirReservation(ModelAndView mv, HttpSession session) {
		mv.setViewName("panelFormateur");
		if(session.getAttribute("formateur")!= null) {
			Personne personne = (Personne) session.getAttribute("formateur");
			Long idPersonne = personne.getId();
			List<Reservation> listReservation = servResMetier.getListReservation(idPersonne);
			if(listReservation != null) {
				mv.addObject("listReservation", listReservation);
				mv.setViewName("listeReservations");
			}
		}
		return mv;
	}
	
	/**
	 * voir le d�tail de la carte de reservation avec toutes les donn�es de reservation
	 * @param mv
	 * @param detailReservation
	 * @return
	 */
	@PostMapping(value="/detailReservation")
	public ModelAndView voirDetailReservation(ModelAndView mv, @RequestParam(value="detailReservation") long detailReservation) {
		Long idReservation = (long) detailReservation;
		System.out.println(idReservation);
		Reservation reservation = servResMetier.getReservation(idReservation);
		System.out.println(reservation.getMotif());
		mv.addObject("reservation", reservation);
		mv.setViewName("detailReservation");
		return mv;
	}
	
	/**
	 * voir le d�tail de la carte de reservation avec toutes les donn�es de reservation
	 * @param mv
	 * @param detailReservation
	 * @return
	 */
	@PostMapping(value="/voirReservationAdmin")
	public ModelAndView voirDetailReservationAdmin(ModelAndView mv, @RequestParam(value="detailReservation") long detailReservation) {
		Long idReservation = (long) detailReservation;
		System.out.println(idReservation);
		Reservation reservation = servResMetier.getReservation(idReservation);
		System.out.println(reservation.getMotif());
		mv.addObject("reservation", reservation);
		mv.setViewName("voirReservationAdmin");
		return mv;
	}
	
	@GetMapping(value="/listeReservation")
	public ModelAndView voirListeReservation(ModelAndView mv, HttpSession session) {
		mv.setViewName("menuAdmin");
		if(session.getAttribute("admin")!= null) {		
			List<Reservation> listReservation = servResMetier.getListeReservations();
			if(listReservation != null) {
				mv.addObject("listReservation", listReservation);
				mv.setViewName("listeReservations");
			}
		}
		return mv;
	}
	
}
