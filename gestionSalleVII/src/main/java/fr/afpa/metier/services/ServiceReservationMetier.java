package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Reservation;
import fr.afpa.entitemetier.Salle;
import fr.afpa.iservicedto.IServiceReservationDto;
import fr.afpa.iservicemetier.IServiceReservationMetier;
import fr.afpa.iservicemetier.IServiceSalleMetier;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;

@Service
public class ServiceReservationMetier implements IServiceReservationMetier{

	@Autowired
	private IServiceReservationDto servResDto;
	
	@Autowired
	private IServiceUtilisateurMetier servUtilMetier;
	
	@Autowired
	private IServiceSalleMetier servSalMetier;
	
	@Override
	public Reservation ajoutReservation(Reservation reservation, Long selectPersonne, Long selectSalle) {
		// TODO Auto-generated method stub
		Personne personne = servUtilMetier.getPersonne(selectPersonne);
		Salle salle = servSalMetier.getSalle(selectSalle);
		reservation.setPersonne(personne);
		
		return servResDto.ajoutReservation(reservation, personne, salle);
	}
	
	
	public List<Reservation> getListReservation(Long id) {
		return servResDto.getListReservation(id);
	}


	@Override
	public Reservation getReservation(Long id) {
		// TODO Auto-generated method stub
		return servResDto.getReservation(id);
	}


	@Override
	public List<Reservation> getListeReservations() {
		// TODO Auto-generated method stub
		return servResDto.getListeReservations();
	}


	@Override
	public Reservation modifierReservation(Reservation reservation, Long idSalle) {
		// TODO Auto-generated method stub
		Salle salle = servSalMetier.getSalle(idSalle);
		
		return servResDto.modifierReservation(reservation, salle);
	}

}
