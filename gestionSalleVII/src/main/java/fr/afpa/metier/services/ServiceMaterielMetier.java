package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import fr.afpa.entitemetier.TypeMateriel;
import fr.afpa.iservicedto.IServiceMaterielDto;
import fr.afpa.iservicemetier.IServiceMaterielMetier;

@Service
public class ServiceMaterielMetier implements IServiceMaterielMetier {
	
	@Autowired
	private IServiceMaterielDto serviceMaterielDto;
	
	@Override
	public TypeMateriel ajoutTypeMateriel(TypeMateriel typeMateriel) {
		return serviceMaterielDto.addTypeMaterielDao(typeMateriel);
	}
	
	@Override
	public List<TypeMateriel> afficherListeMateriels(){
		
		return serviceMaterielDto.afficherListeMateriels();
		
	}

	@Override
	public TypeMateriel getMateriel(Long id) {
		// TODO Auto-generated method stub
		return serviceMaterielDto.getMateriel(id);
	}

	@Override
	public TypeMateriel deleteById(long id) {
		// TODO Auto-generated method stub
		return serviceMaterielDto.deleteMateriel(id);
	}

	@Override
	public TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel) {
		// TODO Auto-generated method stub
		return serviceMaterielDto.updateTypeMateriel(typeMateriel);
	}

	
}
