package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitemetier.Batiment;
import fr.afpa.entitemetier.Materiel;
import fr.afpa.entitemetier.Salle;
import fr.afpa.entitemetier.TypeSalle;
import fr.afpa.iservicedto.IServiceSalleDto;
import fr.afpa.iservicemetier.IServiceSalleMetier;

@Service
public class ServiceSalleMetier implements IServiceSalleMetier {

	@Autowired
	private IServiceSalleDto servSalleDto;

	@Override
	public boolean creationSalle(String nom, Long batiment, Long type, List<Materiel> listeMateriel) {
		Salle salle = new Salle();

		salle.setNom(nom);
		

		Batiment batimentMetier = servSalleDto.rechercheBatiment(batiment);

		TypeSalle typeSalleMetier = servSalleDto.rechercheTypeSalle(type);

		salle.setBatiment(batimentMetier);

		salle.setType(typeSalleMetier);
		salle.setListMateriel(listeMateriel);

		return servSalleDto.creationSalle(salle);
	}

//methode � completer
	@Override
	public boolean modificationSalle(String nom, Long batiment, Long type, boolean reserve) {

		Salle salle = servSalleDto.rechercheSalle(nom);

		Batiment batimentMetier = servSalleDto.rechercheBatiment(batiment);

		TypeSalle typeSalleMetier = servSalleDto.rechercheTypeSalle(type);

		salle.setBatiment(batimentMetier);

		salle.setType(typeSalleMetier);

		// est ce qu'il faut setter le nom ?

		return servSalleDto.modificationSalle(salle, batimentMetier, typeSalleMetier);
	}

	public List<Salle> afficherListeSalle() {

		return servSalleDto.listerToutesLesSalles();
	}

	@Override
	public List<TypeSalle> getListTypeSalle() {
		// TODO Auto-generated method stub
		return servSalleDto.getListSalleDto();
	}

	@Override
	public TypeSalle creationTypeSalle(TypeSalle typeSalle) {
		// TODO Auto-generated method stub
		return servSalleDto.addTypeSalle(typeSalle);
	}

	public Salle afficherSalle(String nom) {

		return servSalleDto.afficherSalle(nom);
	}

	public Salle getSalle(Long id) {
		return servSalleDto.getSalle(id);
	}

	@Override
	public boolean modificationSalle(String nom,Long idSalle, Long batiment, Long type, List<Materiel> listeMateriel) {
		
		Salle salle = new Salle();

		salle.setNom(nom);
		salle.setIdSalle(idSalle);

		Batiment batimentMetier = servSalleDto.rechercheBatiment(batiment);

		TypeSalle typeSalleMetier = servSalleDto.rechercheTypeSalle(type);

		salle.setBatiment(batimentMetier);

		salle.setType(typeSalleMetier);
		salle.setListMateriel(listeMateriel);

		return servSalleDto.modificationSalle(salle);
	}

	@Override
	public TypeSalle getTypeSalle(Long id) {
		// TODO Auto-generated method stub
		return servSalleDto.rechercheTypeSalle(id);
	}

	@Override
	public Salle creationSalleRest(Salle salle) {
		// TODO Auto-generated method stub

		return servSalleDto.creationSalleRest(salle);
	}

	@Override
	public TypeSalle modificationTypeSalle(TypeSalle typeSalle) {
		return servSalleDto.modificationTypeSalle(typeSalle);
	}

	@Override
	public Salle modificationSalle(Salle salle) {
		return servSalleDto.modificationSalleRest(salle);
	}
	

}
