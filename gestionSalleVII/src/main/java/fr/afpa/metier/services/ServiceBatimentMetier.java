package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitemetier.Batiment;
import fr.afpa.iservicedto.IServiceBatimentDto;
import fr.afpa.iservicemetier.IServiceBatimentMetier;

@Service
public class ServiceBatimentMetier implements IServiceBatimentMetier{
	@Autowired
	private IServiceBatimentDto serviceBatimentDto;
	
	
	@Override
	public List<Batiment> getListBatimentMetier() {
		return serviceBatimentDto.getListBatimentDto();	
	}


	@Override
	public Batiment ajoutBatiment(Batiment batiment) {
		// TODO Auto-generated method stub
		return serviceBatimentDto.addBatiment(batiment);
	}


	@Override
	public Batiment getBatiment(Long id) {
		// TODO Auto-generated method stub
		return serviceBatimentDto.getBatiment(id);
	}


	@Override
	public Batiment updateBatiment(Batiment batiment) {
		// TODO Auto-generated method stub
		return serviceBatimentDto.updateBatiment(batiment);
	}

}
