package fr.afpa.metier.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitedao.PersonneDao;


import fr.afpa.entitemetier.Personne;

import fr.afpa.iservicedto.IServiceUtilisateurDto;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;

@Service
public class ServicesUtilisateurMetier implements IServiceUtilisateurMetier {

	@Autowired
	private IServiceUtilisateurDto servDto;

	public List<Personne> afficherListeUtilisateur() {
		return servDto.listerTousLesUtilisateurs();
	}
	
	public PersonneDao modifierUtilisateur(Personne personne) {
		
		return servDto.modifUtilisateur(personne);
	}
	

	@Override
	public Personne creationPersonne(Personne personne) {

		/*Role roleMetier = servDto.rechercheRole(personne.getRole().getIdRole());
		

		if (personne.getRole().getIdRole()==1) {
			personne = new Admin();

		} else if (personne.getRole().getIdRole()==2) {
			personne = new Formateur();

		}

		else if (personne.getRole().getIdRole()==3) {
			personne = new Stagiaire();

		}
		/*personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setEmail(email);
		personne.setTel(tel);
		personne.setAdresse(adresse);
		personne.setAuthentification(authentification);
		personne.setRole(roleMetier);*/
		//authentification.setPersonne(personne);
		
		// System.out.println(authentification);

		return servDto.creationUtilisateur(personne);

	}
	
	public List<Personne> getListFormateur(Long id){
		return servDto.getListFormateur(id);
	}
	
	public Personne getPersonne(Long id) {
		return servDto.getPersonne(id);
	}
/*
	@Override
	public Personne modificationPersonne(Long id,Role role,
			Authentification authentification) {
		

		return servDto.modificationUtilisateur(id,authentification, role);

	}*/

	public Personne modificationPersonne(Personne personne) {
		

		return servDto.modificationUtilisateur(personne);

	}
}
