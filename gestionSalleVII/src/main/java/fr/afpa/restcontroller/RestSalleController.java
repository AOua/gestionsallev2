package fr.afpa.restcontroller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.dao.repositories.SalleRepository;

import fr.afpa.entitemetier.Salle;
import fr.afpa.iservicemetier.IServiceSalleMetier;

@RestController
@CrossOrigin(value="http://localhost:3000")
public class RestSalleController {
	
	@Autowired
	private IServiceSalleMetier servSalleMetier;

	@Autowired
	private SalleRepository salleRepo;
	

	
	@PostMapping(value = "/creationSalle")
	public Salle addSalle(@RequestBody Salle salle) {
		System.out.println(salle.getListMateriel().toString());
		return servSalleMetier.creationSalleRest(salle);

	}
	
	
	@GetMapping(value ="/salles")
	public List<Salle> getSalles() {
		return servSalleMetier.afficherListeSalle();
	}

	@GetMapping(value ="/salles/{id}")
	public Salle getSalle(@PathVariable("id") long id) {
		return servSalleMetier.getSalle(id);
	}
	
	@DeleteMapping(value="/salles/{id}")
	public void deleteSalle(@PathVariable("id") long id) {
		salleRepo.deleteById(id);
	}
	
	@PutMapping(value="/updateSalle")
	public Salle updateSalle(@RequestBody Salle salle) {
		
		return servSalleMetier.modificationSalle(salle);

	
	}
}
