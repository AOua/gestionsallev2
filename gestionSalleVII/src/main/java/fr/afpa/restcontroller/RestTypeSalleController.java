package fr.afpa.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.dao.repositories.TypeSalleRepository;

import fr.afpa.entitemetier.TypeSalle;
import fr.afpa.iservicemetier.IServiceSalleMetier;

@CrossOrigin(value ="http://localhost:3000")
@RestController
public class RestTypeSalleController {

	@Autowired
	private IServiceSalleMetier servSalleMetier;
	
	@Autowired
	private TypeSalleRepository typeSalleRepository;
	
	@PostMapping("/typeSalles")
	public TypeSalle addTypeSalles(@RequestBody TypeSalle typeSalle){
					
		return servSalleMetier.creationTypeSalle(typeSalle);
	
	}	
	@GetMapping("/typeSalles")
	public List<TypeSalle> getAllTypeSalles(){
					
		return servSalleMetier.getListTypeSalle();
	}
	
	@GetMapping("/typeSalles/{id}")
	public TypeSalle getTypeSalle(@PathVariable("id") long id){
		return servSalleMetier.getTypeSalle(id);
	}
	
	@DeleteMapping("/typeSalles/{id}")
	public void  deleteTypeSalle(@PathVariable("id") long id){
		typeSalleRepository.deleteById(id);
	}
	
	@PutMapping("/modifTypeSalle")
	public TypeSalle modifTypeSalle(@RequestBody TypeSalle typeSalle) {
		
		return servSalleMetier.modificationTypeSalle(typeSalle);
	}
}
