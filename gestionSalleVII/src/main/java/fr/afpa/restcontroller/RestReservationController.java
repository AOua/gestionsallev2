package fr.afpa.restcontroller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import fr.afpa.dao.repositories.ReservationRepository;

import fr.afpa.entitemetier.Reservation;
import fr.afpa.iservicemetier.IServiceReservationMetier;



@CrossOrigin(value ="http://localhost:3000")
@RestController
public class RestReservationController {
	
	
	@Autowired
	private IServiceReservationMetier servResMetier;
	
	@Autowired
	private ReservationRepository reservationRepository;

	
	/**
	 * Afficher liste des utilisateurs
	 * @return
	 */
	@GetMapping("/reservations")
	public List<Reservation> getReservations(){
		
		List<Reservation> listReservation = servResMetier.getListeReservations();
			
		return listReservation;
	}
	
	@GetMapping("/reservations/{id}")
	public Reservation getReservation(@PathVariable("id") long id){
					
		Reservation reservation = servResMetier.getReservation(id);
		return reservation;
	}
	
	@PostMapping("/reservations")
	public Reservation addReservation(@RequestBody Reservation reservation){
					
		
		
		Long idPersonne = reservation.getPersonne().getId();
		Long idSalle = reservation.getSalle().getIdSalle();
		
		/*String dateDebut = reservation.getDateDebut().toString();
		DateTimeFormatter  formatterDebut = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldDebut= LocalDate.parse(dateDebut, formatterDebut);
		reservation.setDateDebut(ldDebut);
		String dateFin = reservation.getDateFin().toString();
		DateTimeFormatter  formatterFin = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldFin= LocalDate.parse(dateFin, formatterFin);
		reservation.setDateFin(ldFin);*/
		//if(ldDebut.isBefore(ldFin)) {
		return	servResMetier.ajoutReservation(reservation, idPersonne, idSalle);			
	//}	
}
	@PutMapping("/updateReservations")
		
		public Reservation updateReservation(@RequestBody Reservation reservation){
					
		
		Long idSalle = (long) reservation.getSalle().getIdSalle();
		/*String dateDebut = dateReservationDebut;
		DateTimeFormatter  formatterDebut = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldDebut= LocalDate.parse(dateDebut, formatterDebut);
		reservation.setDateDebut(ldDebut);
		String dateFin = dateReservationFin;
		DateTimeFormatter  formatterFin = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ldFin= LocalDate.parse(dateFin, formatterFin);
		reservation.setDateFin(ldFin);
		if(ldDebut.isBefore(ldFin)) {*/
		return	servResMetier.modifierReservation(reservation, idSalle);	
	
					
	//}	
	}
	
	@DeleteMapping("/deleteReservation/{id}")
	public void  deleteReservation(@PathVariable("id") long id){

		reservationRepository.deleteById(id);
	}

}