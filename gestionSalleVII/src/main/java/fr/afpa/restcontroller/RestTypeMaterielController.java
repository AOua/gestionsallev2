package fr.afpa.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.dao.repositories.TypeMaterielRepository;

import fr.afpa.entitemetier.TypeMateriel;
import fr.afpa.iservicemetier.IServiceMaterielMetier;

@CrossOrigin(value ="http://localhost:3000")
@RestController
public class RestTypeMaterielController {
	@Autowired
	private IServiceMaterielMetier servMateriel;
	
	@Autowired
	TypeMaterielRepository repoTypeMateriel;
	
	@PostMapping("/typeMateriels")
	public TypeMateriel addTypeMateriels(@RequestBody TypeMateriel typeMateriel){
					
		return servMateriel.ajoutTypeMateriel(typeMateriel);
	
	}	
	@GetMapping("/typeMateriels")
	public List<TypeMateriel> getAllTypeMateriels(){
					
		return servMateriel.afficherListeMateriels();
	}
	
	@GetMapping("/typeMateriel/{id}")
	public TypeMateriel getTypeMateriel(@PathVariable("id") long id){
		return servMateriel.getMateriel(id);
	}
	
	
	@DeleteMapping("/typeMateriel/{id}")
	public void  deleteTypeSalle(@PathVariable("id") long id){
	repoTypeMateriel.deleteById(id);
	}
	
	@PutMapping("/updateTypeMateriel")
	public TypeMateriel updateTypeMateriel(@RequestBody TypeMateriel typeMateriel) {
		return servMateriel.updateTypeMateriel(typeMateriel);
	}
	
	
}
