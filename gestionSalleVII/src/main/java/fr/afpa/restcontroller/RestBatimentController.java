package fr.afpa.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.dao.repositories.BatimentRepository;
import fr.afpa.entitemetier.Batiment;
import fr.afpa.iservicemetier.IServiceBatimentMetier;

@CrossOrigin(value ="http://localhost:3000")
@RestController
public class RestBatimentController {
	
	@Autowired
	private IServiceBatimentMetier servBatiment;
	
	
	@Autowired
	private BatimentRepository batimentRepo;
	
	
	@PostMapping("/batiments")
	public Batiment addBatiment(@RequestBody Batiment batiment){
					
		return servBatiment.ajoutBatiment(batiment);
	
	}	
		
	@GetMapping("/batiments")
	public List<Batiment> getListBatiments(){
		return servBatiment.getListBatimentMetier();
	}
	
	@GetMapping("/batiments/{id}")
	public Batiment getBatiment(@PathVariable("id") long id){
		return servBatiment.getBatiment(id);
	}
	
	@DeleteMapping("/batiments/{id}")
	public void deleteBatiment(@PathVariable("id") long id){
		batimentRepo.deleteById(id);
	}
	
	
	@PutMapping("/updateBatiments")
	public Batiment updateBatiment(@RequestBody Batiment batiment){
					
		return servBatiment.updateBatiment(batiment);
	
	}	
}
