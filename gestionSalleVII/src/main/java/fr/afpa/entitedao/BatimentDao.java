package fr.afpa.entitedao;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name="batiment")
public class BatimentDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "batiment_generator")
	@SequenceGenerator(name = "batiment_generator", sequenceName = "batiment_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idBatiment", updatable = false, nullable = false )
	private long idBatiment;
	
	@Column
	private String nom;
	
	@OneToMany(mappedBy = "batiment", fetch=FetchType.EAGER)
	@OnDelete(action=OnDeleteAction.CASCADE)
	private List<SalleDao> listeSalle;

}
