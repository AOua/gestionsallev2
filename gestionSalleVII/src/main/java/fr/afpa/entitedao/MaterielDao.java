package fr.afpa.entitedao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity(name = "materiel")
@IdClass(MaterielId.class)
public class MaterielDao implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@ManyToOne
	@JoinColumn(name="id_typeMateriel")
	private TypeMaterielDao typeMateriel;
	
	@Id
	@ManyToOne
	@JoinColumn(name="idSalle")
	private SalleDao salle;
	
	//@Id
	@Column
	private int quantite;
}
