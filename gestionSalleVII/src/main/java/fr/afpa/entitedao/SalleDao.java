package fr.afpa.entitedao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name="salle")
public class SalleDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "salle_generator")
	@SequenceGenerator(name = "salle_generator", sequenceName = "salle_seq",allocationSize = 1,initialValue = 2)
	@Column
	private Long idSalle;
	    
	
	@Column
	private String nom;
	
	@Column
	private boolean reserve;
	
	@OneToMany(mappedBy = "salle")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<ReservationDao> listReservation; 
	
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "idbatiment")
	private BatimentDao batiment;

	
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name="id_typeSalle")
	private TypeSalleDao typeSalle;
	
	@OneToMany(mappedBy = "salle",cascade = CascadeType.ALL,  fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<MaterielDao> listMateriel;

}
