package fr.afpa.entitedao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity(name="typeMateriel")
public class TypeMaterielDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "type_materiel_generator")
	@SequenceGenerator(name = "type_materiel_generator", sequenceName = "seq_typemateriel",allocationSize = 1,initialValue = 5) //� verifier avec la team 
	private Long id_typeMateriel;
	
	@Column
	private String libelle;
	
	@OneToMany(mappedBy = "typeMateriel", cascade = CascadeType.ALL)
	private List<MaterielDao> listeMateriel;
	
}
