package fr.afpa.entitedao;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


@Entity
@Table(name="reservation")
public class ReservationDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "reservation_generator")
	@SequenceGenerator(name = "reservation_generator", sequenceName = "seq_reservation",allocationSize = 1,initialValue = 2)
	private Long idReservation;
	
	@Column
	private String motif;
	
	@Column(name="dateReservationDebut")
	private LocalDate dateDebut;
	
	@Column(name="dateReservationFin")
	private LocalDate dateFin;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="idPersonne")
	private PersonneDao personne;
	
	@ManyToOne
	@JoinColumn(name="idSalle")
	private SalleDao salle;
}
