package fr.afpa.entitedao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity(name="typeSalle")
public class TypeSalleDao {
	
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "type_salle_generator")
		@SequenceGenerator(name = "type_salle_generator", sequenceName = "seq_typesalle",allocationSize = 1,initialValue = 2) //� verifier avec la team 
		private Long id_typeSalle;
		
		@Column(name="libelle")
		private String libelle;
		
		@OneToMany(mappedBy = "typeSalle", cascade = CascadeType.ALL)
		private List<SalleDao> ListeSalle;
		
		
}
