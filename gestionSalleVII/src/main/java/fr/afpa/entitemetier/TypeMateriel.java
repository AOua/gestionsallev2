package fr.afpa.entitemetier;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class TypeMateriel {

	private Long id;
	private String libelle;
	
	public TypeMateriel() {
		
	}
	
	public TypeMateriel(Long id, String libelle) {
		this.id = id;
		this.libelle = libelle;
	}
}
