package fr.afpa.entitemetier;

public class Stagiaire extends Personne{
	
	public Stagiaire() {
		super();
	}

	public Stagiaire(String nom, String prenom,String email, String tel, String adresse, Authentification authentification, Role role) {
		this.nom=nom;
		this.prenom=prenom;
		this.email=email;
		this.tel=tel;
		this.adresse=adresse;
		this.authentification= authentification;
		this.role=role;
	}
}
