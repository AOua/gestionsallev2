package fr.afpa.entitemetier;

public class Admin extends Personne {

	
	public Admin() {
		super();
	}
	
	public Admin(String nom, String prenom,String email, String tel, String adresse, Authentification authentification, Role role) {
		this.nom=nom;
		this.prenom=prenom;
		this.email=email;
		this.tel=tel;
		this.adresse=adresse;
		this.authentification= authentification;
		this.role=role;
	}
}
