package fr.afpa.entitemetier;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Batiment {

	private Long idBatiment;
	
	private String nom;
	
	private List<Salle> listeSalle;
	
	
	
}
