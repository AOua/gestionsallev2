package fr.afpa.entitemetier;


import lombok.Getter;

import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString



public class Materiel {

	private Long id;
	private String nom;
	private int quantite;
	
	public Materiel() {
		
	}
	
	public Materiel(String nom, int quantite) {
			this.nom = nom;
			this.quantite = quantite;
	}

	public Materiel(Long id, String nom, int quantite) {
		super();
		this.id = id;
		this.nom = nom;
		this.quantite = quantite;
	}
	
	
		
	
}


