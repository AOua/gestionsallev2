package fr.afpa.entitemetier;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class Salle {

	private Long idSalle;
	
	private String nom;
	private TypeSalle type;  //� verifier
	private boolean reserve;
	private List<Materiel> listMateriel;
	private Batiment batiment;
	
	
	//private TypeSalle type; � verifier
	
	
	
}
