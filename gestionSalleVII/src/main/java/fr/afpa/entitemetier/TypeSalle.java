package fr.afpa.entitemetier;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class TypeSalle {
	
	//@NoArgsConstructor
	//@AllArgsConstructor
@Getter
@Setter
	
	private Long id;
	private String libelle;
	private List<Salle> listeSalle;
	
	
	
	public TypeSalle() {
		
	}
	
	public TypeSalle(Long id, String libelle, List<Salle> listeSalle) {
		super();
		this.id = id;
		this.setLibelle(libelle);
		this.setListeSalle(listeSalle);
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Salle> getListeSalle() {
		return listeSalle;
	}

	public void setListeSalle(List<Salle> listeSalle) {
		this.listeSalle = listeSalle;
	}


}
