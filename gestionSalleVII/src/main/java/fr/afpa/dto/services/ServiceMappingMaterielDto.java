package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.entitedao.MaterielDao;
import fr.afpa.entitedao.TypeMaterielDao;
import fr.afpa.entitemetier.Materiel;
import fr.afpa.entitemetier.TypeMateriel;

public class ServiceMappingMaterielDto {

	public static TypeMaterielDao mappingTypeMaterielMetierToTypeMaterielDao(TypeMateriel typeMaterielMetier) {

		TypeMaterielDao typeMaterielDao = new TypeMaterielDao();
		typeMaterielDao.setId_typeMateriel(typeMaterielMetier.getId());
		typeMaterielDao.setLibelle(typeMaterielMetier.getLibelle());

		return typeMaterielDao;
	}
	
	public static TypeMateriel mappingTypeMaterielDaoToTypeMaterielMetier(TypeMaterielDao typeMaterielDao) {

		TypeMateriel typeMateriel = new TypeMateriel();
		typeMateriel.setId(typeMaterielDao.getId_typeMateriel());
		typeMateriel.setLibelle(typeMaterielDao.getLibelle());

		return typeMateriel;
	}

	public static List<TypeMateriel> mappingTypeMaterielDaoToTypeMaterielMetier(List<TypeMaterielDao> listeTypeMaterielDao) {

		List<TypeMateriel> listeTypeMateriel = new ArrayList<TypeMateriel>();
		
		for (TypeMaterielDao typeMaterielDao : listeTypeMaterielDao) {
			TypeMateriel typeMateriel = new TypeMateriel();
			typeMateriel.setId(typeMaterielDao.getId_typeMateriel());
			typeMateriel.setLibelle(typeMaterielDao.getLibelle());
			
			listeTypeMateriel.add(typeMateriel);

		}
		
		return listeTypeMateriel;
	}
	
	public static List<MaterielDao> mappingMaterielMetierToMaterielDao(List<Materiel> listeMaterielMetier) {

		List<MaterielDao> listeMaterielDao = new ArrayList<MaterielDao>();
		
		for (Materiel materiel : listeMaterielMetier) {
			
			MaterielDao materielDao = new MaterielDao();
			materielDao.setQuantite(materiel.getQuantite());
	
			listeMaterielDao.add(materielDao);

		}
		
		return listeMaterielDao;
	}
	
	public static TypeMaterielDao mappingMaterielMetierToTypeMaterielDao(Materiel MaterielMetier) {

		TypeMaterielDao typeMaterielDao = new TypeMaterielDao();
		typeMaterielDao.setId_typeMateriel(MaterielMetier.getId());
		typeMaterielDao.setLibelle(MaterielMetier.getNom());

		return typeMaterielDao;
	}

}
