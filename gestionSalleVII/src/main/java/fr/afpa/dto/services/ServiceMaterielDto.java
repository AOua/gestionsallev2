package fr.afpa.dto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.TypeMaterielRepository;
import fr.afpa.entitedao.TypeMaterielDao;

import fr.afpa.entitemetier.TypeMateriel;
import fr.afpa.iservicedto.IServiceMaterielDto;

@Service
public class ServiceMaterielDto implements IServiceMaterielDto {
	
	@Autowired
	private TypeMaterielRepository typeMaterielRepository;

	/**
	 * Ajouter le type de materiel en bdd
	 */
	public TypeMateriel addTypeMaterielDao(TypeMateriel typeMateriel) {
		TypeMaterielDao typeMaterielDao = ServiceMappingMaterielDto.mappingTypeMaterielMetierToTypeMaterielDao(typeMateriel);
		if(typeMaterielDao != null) {
			typeMaterielRepository.save(typeMaterielDao);
			
		}
		return typeMateriel;
	}
	
	public List<TypeMateriel> afficherListeMateriels(){
		List<TypeMaterielDao> listeMaterielDao = typeMaterielRepository.findAll();
		List<TypeMateriel> listeMateriel = ServiceMappingMaterielDto.mappingTypeMaterielDaoToTypeMaterielMetier(listeMaterielDao);
		return listeMateriel;
		
		
	}

	@Override
	public TypeMateriel getMateriel(Long id) {
		// TODO Auto-generated method stub
		TypeMaterielDao typeMaterielDao = typeMaterielRepository.findById(id).orElse(null);
		TypeMateriel typeMateriel = ServiceMappingMaterielDto.mappingTypeMaterielDaoToTypeMaterielMetier(typeMaterielDao);
		return typeMateriel;
	}

	@Override
	public TypeMateriel deleteMateriel(long id) {
		
		TypeMaterielDao typeMaterielDao = typeMaterielRepository.findById(id).orElse(null);
		TypeMateriel typeMateriel = ServiceMappingMaterielDto.mappingTypeMaterielDaoToTypeMaterielMetier(typeMaterielDao);
		typeMaterielRepository.deleteById(typeMaterielDao.getId_typeMateriel());
		return typeMateriel; 
		
	}

	@Override
	public TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel) {
		// TODO Auto-generated method stub
		TypeMaterielDao typeMaterieldao = typeMaterielRepository.findById(typeMateriel.getId()).get();
		typeMaterieldao.setLibelle(typeMateriel.getLibelle());
		
				typeMaterielRepository.saveAndFlush(typeMaterieldao);
				
				return typeMateriel;
	}
	
}
