package fr.afpa.dto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.BatimentRepository;
import fr.afpa.entitedao.BatimentDao;
import fr.afpa.entitemetier.Batiment;
import fr.afpa.iservicedto.IServiceBatimentDto;

@Service
public class ServiceBatimentDto implements IServiceBatimentDto {
	
	@Autowired
	private BatimentRepository batimentRepository;
	
	/**
	 * Retourner la liste des batiments present en bdd
	 */
	@Override
	public List<Batiment> getListBatimentDto() {
		List<BatimentDao> listBatimentDao = batimentRepository.findAll(); 
		List<Batiment> listBatimentMetier = ServiceMappingSalleDto.mappingListBatimentDaoToListBatimentMetier(listBatimentDao);
		return listBatimentMetier;
	}

	/**
	 * Ajouter un batiment
	 */
	@Override
	public Batiment addBatiment(Batiment batimentMetier) {
		BatimentDao batimentDao = ServiceMappingSalleDto.mappingBatimentMetierToBatimentDao(batimentMetier);
		if(batimentDao != null) {
			batimentRepository.save(batimentDao);
			
		}
		
		return batimentMetier;
	}

	@Override
	public Batiment getBatiment(Long id) {
		// TODO Auto-generated method stub
		BatimentDao batimentDao = batimentRepository.findById(id).orElse(null);
		Batiment batiment = ServiceMappingSalleDto.mappingBatimentDaoToBatimentMetier(batimentDao);
		return batiment;
	}

	@Override
	public Batiment updateBatiment(Batiment batiment) {
		BatimentDao batimentDao = batimentRepository.findById(batiment.getIdBatiment()).orElse(null);
		batimentDao.setNom(batiment.getNom());
		
		batimentRepository.saveAndFlush(batimentDao);
		return batiment;
	}

}
