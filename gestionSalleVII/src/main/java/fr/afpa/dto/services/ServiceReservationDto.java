package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.ReservationRepository;
import fr.afpa.entitedao.PersonneDao;
import fr.afpa.entitedao.ReservationDao;
import fr.afpa.entitedao.SalleDao;
import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Reservation;
import fr.afpa.entitemetier.Salle;
import fr.afpa.iservicedto.IServiceReservationDto;

@Service
public class ServiceReservationDto implements IServiceReservationDto {

	@Autowired
	private ReservationRepository reservationRepo;
	
	/**
	 * Ajout de la reservation dans la base de donn�e
	 */
	@Override
	public Reservation ajoutReservation(Reservation reservation, Personne personne, Salle salle) {
		// TODO Auto-generated method stub
		ReservationDao reservationDao = ServiceMappingReservationDto.mappingReservationToReservationDao(reservation);
		PersonneDao personneDao = ServicesMappingDto.mappingPersonneMetierToPersonneDao(personne);
		SalleDao salleDao = ServiceMappingSalleDto.mappingSalleMetierToSalleDao(salle);
		salleDao.setReserve(true);
		reservationDao.setPersonne(personneDao);
		reservationDao.setSalle(salleDao);
		
		reservationRepo.save(reservationDao);
		
		
		return reservation;
	}
	
	/**
	 * R�cup�rer la liste des reservations de la personne qui se connecte
	 * avec l'id de la personne
	 */
	public List<Reservation> getListReservation(Long id) {
		List<ReservationDao> listReservationDao = reservationRepo.findByPersonneIdPersonne(id);
		
		List<Reservation> listReservation = new ArrayList<Reservation>();
		if(listReservationDao != null) {
			for (ReservationDao reservationDao : listReservationDao) {
				Reservation reservation = ServiceMappingReservationDto.mappingReservationDaoToReservationMetier(reservationDao);
				listReservation.add(reservation);
			}	
		}
		return listReservation;
	}
	
	/**
	 * R�cup�rer la liste des reservations de la personne qui se connecte
	 * avec l'id de la personne
	 */
	public List<Reservation> getListeReservations() {
		List<ReservationDao> listReservationDao = reservationRepo.findAll();
		
		List<Reservation> listReservation = new ArrayList<Reservation>();
		if(listReservationDao != null) {
			for (ReservationDao reservationDao : listReservationDao) {
				Reservation reservation = ServiceMappingReservationDto.mappingReservationDaoToReservationMetier(reservationDao);
				listReservation.add(reservation);
			}	
		}
		return listReservation;
	}

	/**
	 * r�cup�rer la reservation selectionn� avec son id
	 */
	@Override
	public Reservation getReservation(Long id) {
		// TODO Auto-generated method stub
		ReservationDao reservationDao = reservationRepo.findById(id).get();
		Reservation reservation = ServiceMappingReservationDto.mappingReservationDaoToReservationMetier(reservationDao);
		return reservation;
	}

	@Override
	public Reservation modifierReservation(Reservation reservation, Salle salle) {
		// TODO Auto-generated method stub
		ReservationDao reservationDao = reservationRepo.findById(reservation.getIdReservation()).orElse(null);
		PersonneDao personneDao = reservationDao.getPersonne();
		reservationDao = ServiceMappingReservationDto.mappingReservationToReservationDao(reservation);
		SalleDao salleDao = reservationDao.getSalle();
		salleDao = ServiceMappingSalleDto.mappingSalleMetierToSalleDao(salle);
		salleDao.setReserve(true);
		reservationDao.setPersonne(personneDao);
		reservationDao.setSalle(salleDao);
		if(reservationDao != null) {
			reservationRepo.saveAndFlush(reservationDao);
			
		}
		return reservation;
	}

}
