package fr.afpa.dto.services;

import fr.afpa.entitedao.RoleDao;
import fr.afpa.entitemetier.Role;

public class ServiceMappingRole {

	public static Role mappingRoleDaoToRoleMetier(RoleDao roleDao) {
		
		Role role = new Role();
		role.setIdRole(roleDao.getIdRole());
		role.setLibelle(roleDao.getLibelle());
		return role;
	}
	
	public static RoleDao mappingRoleMetiertoRoleDao(Role role) {
		RoleDao roleDao = new RoleDao();
		roleDao.setIdRole(role.getIdRole());
		roleDao.setLibelle(role.getLibelle());
		
		return roleDao;
	}
}
