package fr.afpa.dto.services;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.BatimentRepository;
import fr.afpa.dao.repositories.MaterielRepository;
import fr.afpa.dao.repositories.SalleRepository;
import fr.afpa.dao.repositories.TypeMaterielRepository;
import fr.afpa.dao.repositories.TypeSalleRepository;
import fr.afpa.entitedao.BatimentDao;
import fr.afpa.entitedao.MaterielDao;

import fr.afpa.entitedao.SalleDao;
import fr.afpa.entitedao.TypeMaterielDao;
import fr.afpa.entitedao.TypeSalleDao;
import fr.afpa.entitemetier.Batiment;

import fr.afpa.entitemetier.Salle;

import fr.afpa.entitemetier.TypeSalle;
import fr.afpa.iservicedto.IServiceSalleDto;

@Service
public class ServiceSalleDto implements IServiceSalleDto {

	@Autowired
	private SalleRepository salleRepository;

	@Autowired
	private BatimentRepository batimentRepo;

	@Autowired
	private TypeSalleRepository typeSalleRepo;
	
	@Autowired
	private MaterielRepository materielRepo;
	
	@Autowired
	private TypeMaterielRepository typeMaterielRepo;

	@Override
	public boolean creationSalle(Salle salleMetier) {
		// TODO Auto-generated method stub
		SalleDao salleDao = ServiceMappingSalleDto.mappingSalleMetierToSalleDao(salleMetier);
		BatimentDao batimentDao = ServiceMappingSalleDto.mappingBatimentMetierToBatimentDao(salleMetier.getBatiment());

		TypeSalleDao typeSalleDao = ServiceMappingSalleDto.mappingTypeSalleMetierToTypeSalleDao(salleMetier.getType());
		
		List <MaterielDao> listeMaterielDao = ServiceMappingMaterielDto.mappingMaterielMetierToMaterielDao(salleMetier.getListMateriel());
		

		salleDao.setBatiment(batimentDao);
		salleDao.setTypeSalle(typeSalleDao);
		salleDao.setListMateriel(listeMaterielDao);
		salleRepository.save(salleDao);
		int i = 0;
		for (MaterielDao materielDao : listeMaterielDao) {
			materielDao.setSalle(salleDao);
				
			TypeMaterielDao typeMaterielDao = ServiceMappingMaterielDto.mappingMaterielMetierToTypeMaterielDao(salleMetier.getListMateriel().get(i));
			System.out.println(i);
			System.out.println(salleMetier.getListMateriel().get(i).getId());
			materielDao.setTypeMateriel(typeMaterielDao);
			i++;
			materielRepo.save(materielDao);
		}
		return true;
		}



	public Batiment rechercheBatiment(Long batiment) {
		BatimentDao batDao = batimentRepo.findById(batiment).get();

		Batiment batMetier = new Batiment();

		batMetier = ServiceMappingSalleDto.mappingBatimentDaoToBatimentMetier(batDao);

		return batMetier;
	}

	public TypeSalle rechercheTypeSalle(Long type) {
		TypeSalleDao salDao = typeSalleRepo.findById(type).get();

		TypeSalle typMetier = new TypeSalle();

		typMetier = ServiceMappingSalleDto.mappingTypeSalleDaoToTypeSalleMetier(salDao);

		return typMetier;

	}

	@Override
	public boolean modificationSalle(Salle salleMetier, Batiment batimentMetier, TypeSalle typeSalleMetier) {

		SalleDao salleDao = ServiceMappingSalleDto.mappingSalleMetierToSalleDao(salleMetier);
		BatimentDao batimentDao = ServiceMappingSalleDto.mappingBatimentMetierToBatimentDao(batimentMetier);

		TypeSalleDao typeSalleDao = ServiceMappingSalleDto.mappingTypeSalleMetierToTypeSalleDao(typeSalleMetier);

		salleDao.setBatiment(batimentDao);
		salleDao.setTypeSalle(typeSalleDao);

		salleRepository.saveAndFlush(salleDao);

		return true;
	}

	@Override
	public Salle rechercheSalle(String nom) {

		SalleDao salDao = salleRepository.findByNom(nom); 

		Salle salMetier = new Salle();

		salMetier = ServiceMappingSalleDto.mappingSalletDaoToSalleMetier(salDao);

		return salMetier;

	}

	@Override
	public List<TypeSalle> getListSalleDto() {
		// TODO Auto-generated method stub
		List<TypeSalleDao> listTypeSalleDao = typeSalleRepo.findAll();
		List<TypeSalle> listSalleMetier = ServiceMappingSalleDto.mappingListTypeSalleDaoToListTypeSalleMetier(listTypeSalleDao);
		return listSalleMetier;
	}

	public TypeSalle addTypeSalle(TypeSalle typeSalle) {
		TypeSalleDao typeSalleDao = ServiceMappingSalleDto.mappingTypeSalleMetierToTypeSalleDao(typeSalle);
		if(typeSalleDao != null) {
			typeSalleRepo.save(typeSalleDao);
			
		}
		return typeSalle;
	}
	@Override
	public List<Salle> listerToutesLesSalles() {
		List <SalleDao> listSalleDao = salleRepository.findAll();
		List<Salle> listSalleMetier = ServiceMappingSalleDto.mappinListSalleDaoToListeSalleMetier(listSalleDao);
		return listSalleMetier;
	}

	/**
	 * Recupere une salle en dao pour l'afficher 
	 */
	
	
	
	@Override
	public Salle afficherSalle(String nom) {
		SalleDao salDao = (SalleDao) salleRepository.findByNom(nom);
		Salle salleMetier = ServiceMappingSalleDto.mappingSalleDaoToSalleMetier(salDao);
		
		return salleMetier;
	}
	public Salle getSalle(Long id) {
		Optional<SalleDao> salleDao = salleRepository.findById(id);
		Salle salleMetier = null;
		if(salleDao.isPresent()) {
			salleMetier = ServiceMappingSalleDto.mappinSalleDaoToSalleMetier(salleDao.get());
		}
		
		
		return salleMetier;
	}



	@Override
	public boolean modificationSalle(Salle salle) {
		SalleDao salleDao = salleRepository.findById(salle.getIdSalle()).orElse(null);
		List <MaterielDao> listeMaterielDao = salleDao.getListMateriel();
		int i = 0;
		
		salleDao = ServiceMappingSalleDto.mappingSalleMetierToSalleDao(salle);
		salleDao.setListMateriel(listeMaterielDao);
		BatimentDao batimentDao = salleDao.getBatiment();
		batimentDao = ServiceMappingSalleDto.mappingBatimentMetierToBatimentDao(salle.getBatiment());

		TypeSalleDao typeSalleDao = salleDao.getTypeSalle();
		typeSalleDao = ServiceMappingSalleDto.mappingTypeSalleMetierToTypeSalleDao(salle.getType());
		
		salleDao.setBatiment(batimentDao);
		salleDao.setTypeSalle(typeSalleDao);
		
		for (MaterielDao materielDao : listeMaterielDao) {
			int quantite = salle.getListMateriel().stream()
					.filter((m)->m.getId()==materielDao.getTypeMateriel().getId_typeMateriel())
					.map(m->m.getQuantite()).findFirst().orElse(0);
			
			materielDao.setQuantite(quantite);
			materielDao.setSalle(salleDao);
			
			materielRepo.saveAndFlush(materielDao);
			i++;		
		}
		batimentRepo.saveAndFlush(batimentDao);
		typeSalleRepo.saveAndFlush(typeSalleDao);
		salleRepository.saveAndFlush(salleDao);
		return true;
	}



	@Override
	public Salle creationSalleRest(Salle salleMetier) {
		SalleDao salleDao = ServiceMappingSalleDto.mappingSalleMetierToSalleDao(salleMetier);
		BatimentDao batimentDao = batimentRepo.findById(salleMetier.getBatiment().getIdBatiment()).orElse(null);
		TypeSalleDao typeSalleDao = typeSalleRepo.findById(salleMetier.getType().getId()).orElse(null);
		
		List <MaterielDao> listeMaterielDao = ServiceMappingMaterielDto.mappingMaterielMetierToMaterielDao(salleMetier.getListMateriel());
		

		salleDao.setBatiment(batimentDao);
		salleDao.setTypeSalle(typeSalleDao);
		//listeMaterielDao.forEach(m->m.setSalle(salleDao));
		
		
		salleDao=salleRepository.save(salleDao);
		salleDao.setListMateriel(listeMaterielDao);
		int i = 0;
		for (MaterielDao materielDao : listeMaterielDao) {
			
			
			TypeMaterielDao typeMaterielDao = typeMaterielRepo.findById(salleMetier.getListMateriel().get(i).getId()).orElse(null);

			materielDao.setSalle(salleDao);
			materielDao.setTypeMateriel(typeMaterielDao);
			materielRepo.saveAndFlush(materielDao);
			i++;		
		}
		return salleMetier;
	}



	@Override
	public TypeSalle modificationTypeSalle(TypeSalle typeSalle) {
		TypeSalleDao typeSalleDao = typeSalleRepo.findById(typeSalle.getId()).get();
		typeSalleDao.setLibelle(typeSalle.getLibelle());
		
				typeSalleRepo.saveAndFlush(typeSalleDao);
				
				return typeSalle;
	}



	@Override
	public Salle modificationSalleRest(Salle salle) {
		SalleDao salleDao = salleRepository.findById(salle.getIdSalle()).orElse(null);
		List <MaterielDao> listeMaterielDao = salleDao.getListMateriel();
	
		
		salleDao = ServiceMappingSalleDto.mappingSalleMetierToSalleDao(salle);
		salleDao.setListMateriel(listeMaterielDao);
		BatimentDao batimentDao = batimentRepo.findById(salle.getBatiment().getIdBatiment()).orElse(null);
		
		TypeSalleDao typeSalleDao = typeSalleRepo.findById(salle.getType().getId()).orElse(null);
	
		
		salleDao.setBatiment(batimentDao);
		salleDao.setTypeSalle(typeSalleDao);
		
		for (MaterielDao materielDao : listeMaterielDao) {
			int quantite = salle.getListMateriel().stream()
					.filter((m)->m.getId()==materielDao.getTypeMateriel().getId_typeMateriel())
					.map(m->m.getQuantite()).findFirst().orElse(0);
			
			materielDao.setQuantite(quantite);
			materielDao.setSalle(salleDao);
			
			materielRepo.saveAndFlush(materielDao);
			
		}
		batimentRepo.saveAndFlush(batimentDao);
		typeSalleRepo.saveAndFlush(typeSalleDao);
		salleRepository.saveAndFlush(salleDao);
		return salle;
	}

}
