package fr.afpa.dto.services;


import fr.afpa.entitedao.ReservationDao;

import fr.afpa.entitemetier.Reservation;

public class ServiceMappingReservationDto {

	public static ReservationDao mappingReservationToReservationDao(Reservation reservation) {
		ReservationDao reservationDao = new ReservationDao();
		reservationDao.setIdReservation(reservation.getIdReservation());
		reservationDao.setMotif(reservation.getMotif());
		reservationDao.setDateDebut(reservation.getDateDebut());
		reservationDao.setDateFin(reservation.getDateFin());

		return reservationDao;
	}
	
	public static Reservation mappingReservationDaoToReservationMetier(ReservationDao reservationDao) {
		Reservation reservation = new Reservation();
		reservation.setIdReservation(reservationDao.getIdReservation());
		reservation.setMotif(reservationDao.getMotif());
		reservation.setDateDebut(reservationDao.getDateDebut());
		reservation.setDateFin(reservationDao.getDateFin());
		reservation.setPersonne(ServicesMappingDto.mappingPersonneDaoToPersonneMetier(reservationDao.getPersonne()));
		reservation.setSalle(ServiceMappingSalleDto.mappinSalleDaoToSalleMetier(reservationDao.getSalle()));
		return reservation;
	}
}
