package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.entitedao.BatimentDao;
import fr.afpa.entitedao.MaterielDao;
import fr.afpa.entitedao.SalleDao;
import fr.afpa.entitedao.TypeSalleDao;
import fr.afpa.entitemetier.Batiment;
import fr.afpa.entitemetier.Materiel;
import fr.afpa.entitemetier.Salle;
import fr.afpa.entitemetier.TypeSalle;

public class ServiceMappingSalleDto {

	public static SalleDao mappingSalleMetierToSalleDao(Salle salleMetier) {
		SalleDao salleDao = new SalleDao();
		salleDao.setNom(salleMetier.getNom());
		salleDao.setIdSalle(salleMetier.getIdSalle());
		salleDao.setReserve(salleMetier.isReserve());
		return salleDao;
	}

	public static BatimentDao mappingBatimentMetierToBatimentDao(Batiment batimentMetier) {

		BatimentDao batimentDao = new BatimentDao();

		//batimentDao.setIdBatiment(batimentMetier.getIdBatiment());
		batimentDao.setNom(batimentMetier.getNom());

		return batimentDao;
	}

	public static TypeSalleDao mappingTypeSalleMetierToTypeSalleDao(TypeSalle typeSalleMetier) {
		TypeSalleDao typeSalleDao = new TypeSalleDao();

		typeSalleDao.setId_typeSalle(typeSalleMetier.getId());
		typeSalleDao.setLibelle(typeSalleMetier.getLibelle());

		return typeSalleDao;

	}

	public static Salle mappingSalleDaoToSalleMetier(SalleDao salleDao) {

		Salle salle = new Salle();

		salle.setIdSalle(salleDao.getIdSalle());
		salle.setNom(salleDao.getNom());

		return salle;
	}

	public static Batiment mappingBatimentDaoToBatimentMetier(BatimentDao batimentDao) {

		Batiment batiment = new Batiment();

		batiment.setIdBatiment(batimentDao.getIdBatiment());
		batiment.setNom(batimentDao.getNom());
		batiment.setListeSalle(mappinglisteSalleDaoToListSalleMetier(batimentDao.getListeSalle()));

		return batiment;
	}

	public static TypeSalle mappingTypeSalleDaoToTypeSalleMetier(TypeSalleDao typeSalleDao) {

		TypeSalle typeSalle = new TypeSalle();

		typeSalle.setId(typeSalleDao.getId_typeSalle());
		typeSalle.setLibelle(typeSalleDao.getLibelle());
		// typeSalle.setListeSalle(typeSalleDao.getListeSalle()); � verifiers

		return typeSalle;
	}

	public static Salle mappingSalletDaoToSalleMetier(SalleDao salDao) {

		Salle salle = new Salle();

		salle.setIdSalle(salDao.getIdSalle());
		salle.setNom(salDao.getNom());
		salle.setReserve(salDao.isReserve());
		// salle.setType(salDao.getTypeSalle());
		// salle.setBatiment(salDao.getBatiment());

		return salle;
	}
	
	public static List<Batiment> mappingListBatimentDaoToListBatimentMetier(List<BatimentDao> listBatimentDao){
		ArrayList<Batiment> listBatimentMetier = new ArrayList<Batiment>();
		for (BatimentDao batimentDao : listBatimentDao) {
			Batiment batimentMetier = mappingBatimentDaoToBatimentMetier(batimentDao);
			listBatimentMetier.add(batimentMetier);
		}
		return listBatimentMetier;
	}

	public static List<TypeSalle> mappingListTypeSalleDaoToListTypeSalleMetier(List<TypeSalleDao> listTypeSalleDao){
		ArrayList<TypeSalle> listTypeSalle = new ArrayList<TypeSalle>();
		for (TypeSalleDao typeSalleDao : listTypeSalleDao) {
			TypeSalle typeSalleMetier = mappingTypeSalleDaoToTypeSalleMetier(typeSalleDao);
			listTypeSalle.add(typeSalleMetier);
		}
		return listTypeSalle;
	}
	public static List<Salle> mappinglisteSalleDaoToListSalleMetier(List<SalleDao> listeSalleDao) {
		ArrayList<Salle> listSalleMetier = new ArrayList<Salle>();

		for (SalleDao salleDao : listeSalleDao) {
			Salle salle = mappingSalleDaoToSalleMetier(salleDao);
			listSalleMetier.add(salle);
		}

		return listSalleMetier;
	}

	public static List<SalleDao> mappingListSalleeMetierToListSalleDao(List<Salle> listSalle) {
		ArrayList<SalleDao> listSalleDao = new ArrayList<SalleDao>();

		for (Salle salle : listSalle) {
			SalleDao salleDao = mappingSalleMetierToSalleDao(salle);
			listSalleDao.add(salleDao);
		}

		return listSalleDao;
	}

	// � completer demain 
	public static List<Salle> mappinListSalleDaoToListeSalleMetier(List<SalleDao> listSalleDao) {
		ArrayList<Salle> listSalleMetier = new ArrayList<Salle>();
		for(SalleDao salleDao : listSalleDao) {
			Salle salle = mappingSalleDaoToSalleMetier(salleDao);
			Batiment batiment = new Batiment();
			batiment.setIdBatiment(salleDao.getBatiment().getIdBatiment());
			batiment.setNom(salleDao.getBatiment().getNom());
			TypeSalle typeSalle = new TypeSalle();
			typeSalle.setId(salleDao.getTypeSalle().getId_typeSalle());
			typeSalle.setLibelle(salleDao.getTypeSalle().getLibelle());
			salle.setBatiment(batiment);
			salle.setType(typeSalle);
			List<Materiel> listMateriel = new ArrayList<Materiel>();
			for (MaterielDao materielDao : salleDao.getListMateriel()) {
				Materiel materiel = new Materiel();
				materiel.setId(materielDao.getTypeMateriel().getId_typeMateriel());
				materiel.setNom(materielDao.getTypeMateriel().getLibelle());
				materiel.setQuantite(materielDao.getQuantite());
				listMateriel.add(materiel);
			}
			salle.setListMateriel(listMateriel);
			listSalleMetier.add(salle);
		}
		return listSalleMetier;
	}
	
	public static Salle mappinSalleDaoToSalleMetier(SalleDao salleDao) {

			Salle salle = mappingSalleDaoToSalleMetier(salleDao);
			Batiment batiment = new Batiment();
			batiment.setIdBatiment(salleDao.getBatiment().getIdBatiment());
			batiment.setNom(salleDao.getBatiment().getNom());
			TypeSalle typeSalle = new TypeSalle();
			typeSalle.setId(salleDao.getTypeSalle().getId_typeSalle());
			typeSalle.setLibelle(salleDao.getTypeSalle().getLibelle());
			salle.setBatiment(batiment);
			salle.setType(typeSalle);
			List<Materiel> listMateriel = new ArrayList<Materiel>();
			for (MaterielDao materielDao : salleDao.getListMateriel()) {
				Materiel materiel = new Materiel();
				materiel.setId(materielDao.getTypeMateriel().getId_typeMateriel());
				materiel.setNom(materielDao.getTypeMateriel().getLibelle());
				materiel.setQuantite(materielDao.getQuantite());
				listMateriel.add(materiel);
			}
			salle.setListMateriel(listMateriel);
		return salle;
	}
	
	public static SalleDao mappinSalleMetierToSalleDao(Salle salle) {

		SalleDao salleDao = mappingSalleMetierToSalleDao(salle);
		BatimentDao batimentDao = new BatimentDao();
		batimentDao.setIdBatiment(salle.getBatiment().getIdBatiment());
		batimentDao.setNom(salle.getBatiment().getNom());
		TypeSalleDao typeSalleDao = new TypeSalleDao();
		typeSalleDao.setId_typeSalle(salle.getType().getId());
		typeSalleDao.setLibelle(salle.getType().getLibelle());
		salleDao.setBatiment(batimentDao);
		salleDao.setTypeSalle(typeSalleDao);
		
	return salleDao;
}
	
}
