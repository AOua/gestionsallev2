<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
<body>



<div class="container-fluid bg">
	<p>Nom de la reservation : ${ reservation.motif } </p>
	<p>Reserv� par : ${ reservation.personne.nom } ${ reservation.personne.prenom } le <javatime:format pattern="dd/MM/yyyy" value="${ reservation.dateDebut}" /> au <javatime:format pattern="dd/MM/yyyy" value="${ reservation.dateFin}" /></p>
	<p>La salle reserv�e est : ${ reservation.salle.nom } de type : ${ reservation.salle.type.libelle }</p>
	<p>La salle se situe dans le batiment ${ reservation.salle.batiment.nom }</p>
	<p>La salle contient :</p>
	<ul>
	<c:forEach items = "${ reservation.salle.listMateriel }" var="materiel">
		<li>${ materiel.quantite } de ${ materiel.nom }</li>
	</c:forEach>
	</ul>
	
	 <form action="modifierReservationAdminForm" method="post">
		<input type="hidden" name="idReservation" value="${ reservation.idReservation }">
		<button class="btn btn-primary" type="submit">Modifier</button>
	  </form>
</div>

<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>


</body>
</html>
