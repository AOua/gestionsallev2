<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<title>Visualisation salle</title>
</head>
<body>



	<div class="container">
		<br />
		<h3>Salle en detail</h3>
		<br />
		<table class="table-bordered">
			<thead class="table-primary">
				<tr>
					<td>Nom</td>
					<td>Type</td>
					<td>Batiment</td>
					<td>Reservee</td>

				</tr>
			</thead>
			<tbody>

				<c:if test="${ not empty salle }">

					<c:forEach var="Salle" items="${salles}">

						<tr>
							<td><c:out value="${Salle ['value'] ['nom'] } " /></td>
							<td><c:out value="${Salle ['value'] ['type'] } " /></td>
							<td><c:out value="${Salle ['value'] ['batiment'] } " /></td>
							<td><c:out value="${Salle ['value'] ['reserve'] } " /></td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		<br />
	</div>

</body>
</html>