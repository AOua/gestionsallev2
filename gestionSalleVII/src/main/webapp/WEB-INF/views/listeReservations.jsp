<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
<body>



<div class="container-fluid bg">
<div class="row">
<c:if test="${ not empty listReservation }">
	<c:forEach items="${ listReservation }" var="reservation">
		
		<div class="col-sm-4">
		<div class="card" style="width: 18rem;">
		  <img class="card-img-top" src="${pageContext.request.contextPath}/resources/image/salle.jpg" alt="une salle">
			  <div class="card-body">
			    <h5 class="card-title">${ reservation.motif }</h5>
			    <p class="card-text">La salle ${ reservation.salle.nom } a �t� reserv�</p>
			    <form action="voirReservationAdmin" method="post">
			    	<input type="hidden" name="detailReservation" value="${ reservation.idReservation }">
			    	<button class="btn btn-primary" type="submit">Voir le d�tail</button>
			    </form>
			  </div>
		  </div>
		  </div>
		
	</c:forEach>
</c:if>
</div>
</div>

<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>


</body>
</html>
