<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
<title>Voir les utilisateurs</title>
</head>
<body>
	<h1>Details utilisateurs</h1>
	<table class="table table-striped" id="table1">
	<thead>
		<tr>
			<th>Id</th>
			<th>Role</th>
			<th>Nom</th>
			<th>Prénom</th>
			<th>Mail</th>
			<th>Tel</th>
			<th>Adresse</th>
			
			<th>Login</th>
			<th>Mot de passe</th>
		</tr>
	</thead>
	<tbody>	
			<tr>
				<th><c:out value=" ${ personne.id } "></c:out></th>
				<th><c:out value=" ${ personne.role.libelle } "></c:out></th>
				<th><c:out value=" ${ personne.nom }" /></th>
				<th><c:out value=" ${ personne.prenom }" /></th>
				<th><c:out value=" ${ personne.email } "></c:out></th>
				<th><c:out value=" ${ personne.tel }" /></th>
				<th><c:out value=" ${ personne.adresse }" /></th>
				<th><c:out value=" ${ personne.authentification.login } "></c:out></th>
				<th><c:out value=" ${ personne.authentification.motDePasse } "></c:out></th>
			</tr>

		
		</tbody>
		</table>
		<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>