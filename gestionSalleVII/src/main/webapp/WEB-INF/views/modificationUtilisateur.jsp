<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>modification</title>
</head>
<body>
	<form id="formmodification" method="post"
		action="m" class="form-container">
		<em class="text-danger"> ${KO}</em>

		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="role" id="admin"
				value="1"> <label class="form-check-label"
				for="inlineRadio1">Admin</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="role"
				id="formateur" value="2"> <label class="form-check-label"
				for="inlineRadio2">Formateur</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="role"
				id="stagiaire" value="3"> <label class="form-check-label"
				for="inlineRadio3">Stagiaire </label>
		</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="nom">Nom</label> <input type="text" class="form-control"
					id="nom" placeholder="Nom" name="nom" value = "<c:if test="${ not empty personne.nom }"><c:out value="${ personne.nom }"></c:out></c:if>" required>
				<em id="nomErr" class="text-danger">${nomKO } </em>

			</div>
			<div class="form-group col-md-6">
				<label for="prenom">Prenom</label> <input type="text"
					class="form-control" id="prenom" placeholder="Prenom" name="prenom"
					value = "<c:if test="${ not empty personne.prenom }"><c:out value="${ personne.prenom }"></c:out></c:if>" required> <em id="prenomErr"
					class="text-danger">${prenomKO } </em>

			</div>
			<div class="form-group col-md-6">
				<label for="email">Email</label> <input type="email"
					class="form-control" id="mail" placeholder="Email" name="mail"
					value = "<c:if test="${ not empty personne.email }"><c:out value="${ personne.email }"></c:out></c:if>" required> <em id="emailErr"
					class="text-danger"> ${mailKO } </em>

			</div>
			<div class="form-group col-md-6">
				<label for="tel">Telephone</label> <input type="text"
					class="form-control" id="tel" placeholder="Telephone" name="tel"
					value = "<c:if test="${ not empty personne.tel }"><c:out value="${ personne.tel }"></c:out></c:if>" required> <em id="telErr"
					class="text-danger">${telKO } </em>

			</div>
			<div class="form-group col-md-6">
				<label for="adresse">Adresse</label> <input type="text"
					class="form-control" id="adresse" placeholder="Adresse"
					name="adresse" value = "<c:if test="${ not empty personne.adresse }"><c:out value="${ personne.adresse }"></c:out></c:if>" required> <em
					id="adresseErr" class="text-danger">${adresseKO } </em>

			</div>

			<div class="form-group col-md-6">
				<label for="login">Login</label> <input type="text"
					class="form-control" id="login" placeholder="Login"
					placeholder="Login" name="login" value = "<c:if test="${ not empty personne.authentification.login }"><c:out value="${ personne.authentification.login }"></c:out></c:if>" required>
				<em id="loginErr" class="text-danger">${loginKO } </em>
			</div>
			<div class="form-group col-md-6">
				<label for="mdp">Mot de passe</label> <input type="password"
					class="form-control" id="mdp" placeholder="Mot de passe" name="mdp"
					value = "<c:if test="${ not empty personne.authentification.motDePasse }"><c:out value="${ personne.authentification.motDePasse }"></c:out></c:if>" required> <em id="mdpErr"
					class="text-danger"> ${mdpKO } </em>
			</div>
			<div class="form-group">
   
  </div>
		</div>
		<div class="form-group">
    <input type="text" type="hidden"   name="id" value="${personne.id }">
     
  </div>
		
		<div class="form-group"></div>
		<button id="valider" type="submit" class="btn btn-primary">Valider</button>
		<br> <a class="nav-item nav-link" href="/gestionSalleVII">Retour</a>
	</form>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
	


</body>
</html>