<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
<title>Liste des salles</title>
</head>
<body>
	<h1>Liste des salles</h1>
	<table class="table table-striped" id="table1">
	<thead>
		<tr>
			
			<th>Nom</th>
			<th>Type</th>
			<th>Batiment</th>
			<th>Reservee</th>
			
		</tr>
	</thead>
	<tbody>	
		<c:forEach items="${ salles }" var="salle">

			<tr>
			
				<th><c:out value=" ${ salle.nom } "></c:out></th>
				<th><c:out value=" ${ salle.type.libelle } "></c:out></th>
				<th><c:out value=" ${ salle.batiment.nom }" /></th>
				<th><c:out value=" ${ salle.reserve }" /></th>
				
				<th>
				<div class="btn-group" role="group">
					<form class="form" action="afficherSalle" method="post">
						<input hidden="" name="id" value="${ salle.idSalle}"/>
						<input class="btn btn-primary" type="submit" value="voir"/>
					</form>
					<form class="form" action="modificationSalle" method="post">
				      	<input type="hidden" name = "id" value="<c:out value="${ salle.idSalle }" />">
				      	<input type="submit" class="btn btn-light" name = "modifier" value="modifier">
			      	</form>
			      	<form class="form" action="Delete" method="post">
				      	<input type="hidden" name = "id" value="<c:out value="${ salle.idSalle }" />">
				      	<input type="submit" class="btn btn-danger" name = "supprimer" value="supprimer">
			      	</form>
				</div>
				</th>
			</tr>

		</c:forEach>
		</tbody>
		</table>
		<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>