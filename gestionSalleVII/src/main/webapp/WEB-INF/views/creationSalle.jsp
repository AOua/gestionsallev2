<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<form id="formCreationSalle" method="post"
		action="/gestionSalleVII/creationSalle">

		<div class="form-group col-md-6">
			<label for="nom">Nom de la salle</label> <input type="text"
				class="form-control" id="nom" placeholder="Nom de la salle"
				name="nom" value="${nom }" required>


		</div>

		<%-- 	<div class="form-group col-md-6">
				<label for="batiment">Batiment</label> <input  class="form-control"
					id="batiment" placeholder="Numero du batiment" name="batiment" value="${batiment }" required>
			</div>--%>

		<div class="form-group col-md-6">
			<label for="selectBatiment">Batiment :</label> 
			<select
				class="form-control" id="selectBatiment" name="selectBatiment">
				<c:forEach items="${ listBatiment }" var="batiment">
					<option value="${ batiment.idBatiment }">${ batiment.nom }</option>
				</c:forEach>
			</select>
		</div>

		<div class="form-group col-md-6">
			<label for="selectTypeSalle">Type de salle :</label> <select
				class="form-control" id="selectTypeSalle" name="selectTypeSalle">
				<c:forEach items="${ listTypeSalle}" var="typeSalle">
					<option value="${ typeSalle.id }">${ typeSalle.libelle }</option>
				</c:forEach>
			</select>
		</div>


		<c:forEach items="${ listTypeMateriel}" var="typeMateriel">
			<div class="form-group col-md-6">
				<label for="nombreMateriel">${ typeMateriel.libelle }</label> 
				<input type="number" min="0" class="form-control" name="listeNombreMateriel[]"/>
				<input type="hidden" name="listeNomMateriel[]" value="${ typeMateriel.libelle }">
				<input type="hidden" name="listeIdMateriel[]" value="${ typeMateriel.id }">
			</div>
		</c:forEach>





		<%-- 
		< div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="reserve"
				id="reserve" value="true"> <label class="form-check-label"
				for="inlineRadio1">Reservee</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="reserve"
				id="libre" value="false"> <label class="form-check-label"
				for="inlineRadio2">Libre</label>
		</div> --%>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>