<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<form id="formCreationReservation" method="post"
		action="/gestionSalleVII/creationReservation">

		<div class="form-group col-md-6">
			<label for="nom">Nom de la reservation</label> 
			<input type="text" class="form-control" id="nom" placeholder="Nom de la reservation" name="nom" value="${ nom }" required>
		</div>

		
		<div class="form-group col-md-6">
			<label for="dateReservationDebut">Date de d�but :</label> 
			<input type="date" class="form-control" id="dateReservationDebut" name="dateReservationDebut"  required>
		</div>
		
		<div class="form-group col-md-6">
			<label for="dateReservationFin">Date de fin :</label> 
			<input type="date" class="form-control" id="dateReservationFin" name="dateReservationFin"  required>
		</div>
	

		<div class="form-group col-md-6">
			<label for="selectPersonne">Pour qui :</label> <select
				class="form-control" id="selectPersonne" name="selectPersonne">
				<c:forEach items="${ listPersonne }" var="personne">
					<option value="${ personne.id }">${ personne.prenom }</option>
				</c:forEach>
			</select>
		</div>

		<div class="form-group col-md-6">
			<label for="selectSalle">Type de salle :</label> <select
				class="form-control" id="selectSalle" name="selectSalle">
				<c:forEach items="${ listSalle }" var="salle">
					<option value="${ salle.idSalle }">${ salle.nom } - ${ salle.type.libelle }</option>
				</c:forEach>
			</select>
		</div>
	
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>