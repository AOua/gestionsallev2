<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<form id="formModificationSalle" method="post"
		action="/gestionSalleVII/modificationSalleForm">

		<div class="form-group col-md-6">
			<label for="nom">Nom de la salle</label> <input type="text"
				class="form-control" id="nom" placeholder="Nom de la salle"
				name="nom" value="<c:out value="${ salle.nom }"></c:out>" required>


		</div>

		<%-- 	<div class="form-group col-md-6">
				<label for="batiment">Batiment</label> <input  class="form-control"
					id="batiment" placeholder="Numero du batiment" name="batiment" value="${batiment }" required>
			</div>--%>

		<div class="form-group col-md-6">
			<label for="selectBatiment">Batiment :</label> 
			<select
				class="form-control" id="selectBatiment" name="selectBatiment" >
				<c:forEach items="${ listBatiment }" var="batiment">
					<c:choose>
					 <c:when test="${ batiment.nom eq salle.batiment.nom }">
					<option value="${ batiment.idBatiment }" selected>${ batiment.nom }</option>
					 </c:when>
					 <c:otherwise>
						<option value="${ batiment.idBatiment }">${ batiment.nom }</option>
					 </c:otherwise>		
					 </c:choose>
				</c:forEach>
			</select>
		</div>

		<div class="form-group col-md-6">
			<label for="selectTypeSalle">Type de salle :</label> 
			<select class="form-control" id="selectTypeSalle" name="selectTypeSalle">
				<c:forEach items="${ listTypeSalle}" var="typeSalle">
					<c:choose>
					 <c:when test="${ typeSalle.libelle eq  salle.type.libelle }">
						<option value="${ typeSalle.id }" selected>${ typeSalle.libelle }</option>
					 </c:when>
					 <c:otherwise>
						<option value="${ typeSalle.id }">${ typeSalle.libelle }</option>
					 </c:otherwise>		
					 </c:choose>
				</c:forEach>
			</select>
		</div>
	

		<c:forEach items="${ salle.listMateriel }" var="typeMateriel">
			<div class="form-group col-md-6">
				<label for="nombreMateriel">${ typeMateriel.nom }</label> 
				<input type="number" min="0" class="form-control" name="listeNombreMateriel[]" value="${ typeMateriel.quantite }"/>
				<input type="hidden" name="listeNomMateriel[]" value="${ typeMateriel.nom }">
				<input type="hidden" name="listeIdMateriel[]" value="${ typeMateriel.id }">
			</div>
		</c:forEach>


	<input type=hidden value="${ salle.idSalle }" name="idsalle">


		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>