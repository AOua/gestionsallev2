<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<form id="formCreationTypeMateriel" method="post"
		action="/gestionSalleVII/creationTypeMateriel">
		
		<div class="form-group col-md-6">
				<label for="nom">Nom du Materiel</label> <input type="text" class="form-control"
					id="nom" placeholder="Nom du materiel" name="nom" value="${nom }" required>
				

		</div>
			
		
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>