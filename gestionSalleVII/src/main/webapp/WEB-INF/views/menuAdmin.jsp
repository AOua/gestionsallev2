<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
</head>
<body>



<div class="container-fluid bg">
	<div class="row justify-content-center">
		<div class="container-fluid col-sm-5">
		${sessionScope.admin.prenom}
			<h2>Bienvenue !</h2>
			<ul class="list-group">
			  <li class="list-group-item list-group-item-dark"><a href="creation">Cr�er un utilisateur</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="listerPersonnes">Lister tous les utilisateurs</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="creationSalle">Cr�er une salle</a></li>
			  
			  <li class="list-group-item list-group-item-dark"><a href="afficherListeSalle">Lister toutes les salles</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="creationTypeMateriel">Cr�er un type de materiel</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="creationTypeSalle">Cr�er un type de salle</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="creationBatiment">Cr�er un batiment</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="ajoutMaterielSalle">Ajouter des materiels dans une salle</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="reserver">Reserver une salle</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="listeReservation">Lister toutes les r�servations</a></li>
			</ul>
		</div>	
	</div>
	<a href="deconnexion" class="btn btn-danger">Deconnexion</a>
</div>

<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>


</body>
</html>
