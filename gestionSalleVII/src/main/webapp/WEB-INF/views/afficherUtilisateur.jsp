<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<title>Visualisation Utilisateur</title>
</head>
<body>

	
	
<div class="container">
	<br/>
	<h3>Affichage details </h3>
		<br/>
		<table class="table-bordered">
			<thead class="table-primary">
				<tr>
					<td>Nom</td>
					<td>Prenom</td>
					<td>Tel</td>
					<td>Mail</td>
					<td>Role</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="Personne" items="${Personne}">

					<tr>
						<td><c:out value="${Personne ['value'] ['nom'] } " /></td>
						<td><c:out value="${Personne ['value'] ['prenom'] } " /></td>
						<td><c:out value="${Personne ['value'] ['tel'] } " /></td>
						<td><c:out value="${Personne ['value'] ['email'] } " /></td>
						<td><c:out value="${Personne ['value'] ['role'] } " /></td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	<br />
</div>

</body>
</html>