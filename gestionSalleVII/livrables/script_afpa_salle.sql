create table roleprofil (
idRole int not null,
libelle varchar not null,
constraint role_pk primary key (idRole)
);

create table typeMateriel(
id_typeMateriel int not null,
libelle varchar not null,
constraint role_pk2 primary key (id_typeMateriel)
);

create table typeSalle(
id_typeSalle int not null,
libelle varchar(50) not null,
constraint type_salle_pk primary key (id_typeSalle)
);

create table batiment(
idBatiment int not null,
nom varchar(50) not null,
constraint batiment_pk primary key (idBatiment)
);

create table salle(
idSalle int not null,
nom varchar(50) not null,
reserve boolean not null,
idBatiment int not null,
id_typeSalle int not null,
constraint salle_pk primary key (idSalle),
constraint batiment_fk foreign key (idBatiment) references batiment(idBatiment),
constraint type_salle_fk foreign key (id_typeSalle) references typeSalle(id_typeSalle)
);



create table materiel(
quantite int null,
idSalle int not null,
id_typeMateriel int not null,
unique(quantite, idSalle, id_typeMateriel),
constraint salle_fk foreign key (idSalle) references salle(idSalle),
constraint type_materiel_fk foreign key (id_typeMateriel) references typeMateriel(id_typeMateriel)
);

create table personne (
idPersonne int not null,
nom varchar(50) not null,
prenom varchar(50) not null,
telephone varchar(50) not null,
email varchar(255) not null,
adresse varchar(255) not null,
idRole int not NULL,
CONSTRAINT profil_fk FOREIGN KEY (idRole) REFERENCES roleprofil(idRole),
constraint personne_pk primary key (idPersonne)
);

create table authentification (
idAuthentification int not null,
login varchar not null,
password varchar not null,
idPersonne int,
constraint authentification_pk primary key (idAuthentification),
CONSTRAINT authentification_fk FOREIGN KEY (idPersonne) REFERENCES personne(idPersonne)
);

create table reservation (
idReservation int not null,
motif varchar(50) not null,
dateReservationDebut date not null,
dateReservationFin date NOT NULL,
idPersonne int not null,
idSalle int not null,
constraint res_pk primary key (idReservation),
CONSTRAINT res_fk FOREIGN KEY (idPersonne) REFERENCES personne(idPersonne),
CONSTRAINT res_fk2 FOREIGN KEY (idSalle) REFERENCES salle(idSalle)
);

insert into roleprofil(idRole, libelle)values(1,'administrateur');
insert into roleprofil(idRole, libelle)values(2,'formateur');
insert into roleprofil(idRole, libelle)values(3,'stagiaire');

insert into personne values(1,'leon', 'charles','0610622386','charles.leo7@gmail.com','55 bis boulevard de la liberte Lille',1);
INSERT INTO personne values(2,'delacroix','gaetan','0606060606','gaetan.delacroix@gmail.com','je sais pas',3);
INSERT INTO personne values(3,'benjira','mohamed','0201030405','mohamed.benjira@gmail.com','retourne a singapour',2);
insert into authentification values(1,'admin','admin',1);
INSERT INTO authentification values(2,'gaetan','123',2);
INSERT INTO authentification values(3,'mohamed','momoformateur',3;)

insert into batiment values(1,'batiment principal');
INSERT INTO batiment values(2,'la maison de Fayaz');
insert into typeSalle values (1,'salle informatique');
INSERT INTO typeSalle VALUES (2,'le dortoir de Seti');
INSERT INTO salle VALUES(2,'Ali baba et les 40 dormeurs',FALSE,2,2);
insert into salle values(1,'salle cda',false,1,1);

insert into typeMateriel values(1,'chaise');
insert into typeMateriel values(2,'table');
insert into typeMateriel values(3,'ordinateur');
insert into typeMateriel values(4,'casier');
insert into materiel values(15,1,1);
insert into materiel values(20,1,2);
insert into materiel values(18,1,3);
insert into materiel values(4,1,4);